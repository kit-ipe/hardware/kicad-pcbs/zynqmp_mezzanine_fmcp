EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 28
Title "ZynqMP Mezzanine FMC+ (BuZZyBoard)"
Date "2020-10-02"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "Institute for Data Processing and Electronics (IPE)"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7000 3250 1100 850 
U 5F7224E6
F0 "PS_MGT" 50
F1 "sch/ps_mgt.sch" 50
$EndSheet
$Sheet
S 5550 2700 1250 650 
U 5DEF958D
F0 "CONFIG" 50
F1 "sch/config.sch" 50
$EndSheet
$Sheet
S 5550 3550 1250 550 
U 5E09AE40
F0 "PWR" 50
F1 "sch/pwr.sch" 50
$EndSheet
$Sheet
S 8450 4350 1000 1100
U 5EDEBADA
F0 "DDR" 50
F1 "sch/ddr.sch" 50
$EndSheet
$Sheet
S 4050 1000 1300 550 
U 5DEE0859
F0 "CLOCKS" 50
F1 "sch/clocks.sch" 50
$EndSheet
$Sheet
S 7000 1800 1100 1250
U 5F72F5D7
F0 "MIO" 50
F1 "sch/mio.sch" 50
$EndSheet
$Comp
L Mechanical:MountingHole H2
U 1 1 5E42431F
P 1000 7200
F 0 "H2" H 1100 7246 50  0000 L CNN
F 1 "M2.5_Pad" H 1100 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 1000 7200 50  0001 C CNN
F 3 "~" H 1000 7200 50  0001 C CNN
	1    1000 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5E424A3B
P 1650 7000
F 0 "H3" H 1750 7046 50  0000 L CNN
F 1 "M2.5" H 1750 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1650 7000 50  0001 C CNN
F 3 "~" H 1650 7000 50  0001 C CNN
	1    1650 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5E424A41
P 1650 7200
F 0 "H4" H 1750 7246 50  0000 L CNN
F 1 "M2.5" H 1750 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1650 7200 50  0001 C CNN
F 3 "~" H 1650 7200 50  0001 C CNN
	1    1650 7200
	1    0    0    -1  
$EndComp
Text Notes 850  6800 0    98   ~ 20
Mounting Holes
$Comp
L Mechanical:Fiducial FID1
U 1 1 5E7DB8DE
P 2750 7000
F 0 "FID1" H 2835 7046 50  0000 L CNN
F 1 "Fiducial" H 2835 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2750 7000 50  0001 C CNN
F 3 "~" H 2750 7000 50  0001 C CNN
	1    2750 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID3
U 1 1 5E7DBFC6
P 3250 7000
F 0 "FID3" H 3335 7046 50  0000 L CNN
F 1 "Fiducial" H 3335 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3250 7000 50  0001 C CNN
F 3 "~" H 3250 7000 50  0001 C CNN
	1    3250 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID5
U 1 1 5E7DC2D6
P 3750 7000
F 0 "FID5" H 3835 7046 50  0000 L CNN
F 1 "Fiducial" H 3835 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3750 7000 50  0001 C CNN
F 3 "~" H 3750 7000 50  0001 C CNN
	1    3750 7000
	1    0    0    -1  
$EndComp
Text Notes 3050 6800 0    98   ~ 20
Fiducials
$Comp
L KIT_Graphic:KIT_LOGO G1
U 1 1 5E7E615F
P 9800 6250
F 0 "G1" H 9800 5986 60  0001 C CNN
F 1 "KIT_LOGO" H 9800 6514 60  0001 C CNN
F 2 "KIT_Graphic:KIT_LOGO_MASK_4.1x1.6mm_Cu" H 9800 6250 157 0001 C CNN
F 3 "" H 9800 6250 157 0001 C CNN
	1    9800 6250
	1    0    0    -1  
$EndComp
$Comp
L KIT_Graphic:LA_LOGO G2
U 1 1 5E7E8B14
P 10800 6200
F 0 "G2" H 11175 6200 50  0001 L CNN
F 1 "LA_LOGO" H 10800 5940 50  0001 C CNN
F 2 "KIT_Graphic:LA_LOGO_MASK_Cu" H 10820 5870 50  0001 C CNN
F 3 "" H 10800 6200 50  0001 C CNN
	1    10800 6200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 5E7F77BC
P 2750 7200
F 0 "FID2" H 2835 7246 50  0000 L CNN
F 1 "Fiducial" H 2835 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2750 7200 50  0001 C CNN
F 3 "~" H 2750 7200 50  0001 C CNN
	1    2750 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 5E7F77C2
P 3250 7200
F 0 "FID4" H 3335 7246 50  0000 L CNN
F 1 "Fiducial" H 3335 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3250 7200 50  0001 C CNN
F 3 "~" H 3250 7200 50  0001 C CNN
	1    3250 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID6
U 1 1 5E7F77C8
P 3750 7200
F 0 "FID6" H 3835 7246 50  0000 L CNN
F 1 "Fiducial" H 3835 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3750 7200 50  0001 C CNN
F 3 "~" H 3750 7200 50  0001 C CNN
	1    3750 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5E423097
P 1000 7000
F 0 "H1" H 1100 7046 50  0000 L CNN
F 1 "M2.5_Pad" H 1100 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 1000 7000 50  0001 C CNN
F 3 "~" H 1000 7000 50  0001 C CNN
	1    1000 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Heatsink #HS1
U 1 1 5E7A3C9C
P 5000 7150
F 0 "#HS1" H 5142 7271 50  0001 L CNN
F 1 "Heatsink" H 5142 7180 50  0000 L CNN
F 2 "" H 5012 7150 50  0001 C CNN
F 3 "https://www.qats.com/DataSheet/ATS-X51310D-C1-R0" H 5012 7150 50  0001 C CNN
F 4 "ATS36780-ND" H 5000 7150 50  0001 C CNN "Digi-Key"
F 5 "ATS1935-ND" H 5000 7150 50  0001 C CNN "Digi-Key2"
	1    5000 7150
	1    0    0    -1  
$EndComp
Text Notes 4700 6800 0    98   ~ 20
Heatsink
$Sheet
S 5550 1000 500  550 
U 5F28C5AE
F0 "IPMI" 50
F1 "sch/ipmi.sch" 50
$EndSheet
Wire Notes Line
	8250 1650 8250 5650
Wire Notes Line
	8250 5650 3900 5650
Wire Notes Line
	3900 5650 3900 1650
Wire Notes Line
	3900 1650 8250 1650
$Sheet
S 7000 1000 1100 550 
U 5F439249
F0 "QSPI_FLASH" 50
F1 "sch/qspi_flash.sch" 50
$EndSheet
$Sheet
S 8450 1800 1000 450 
U 5FA2C9D2
F0 "USB_PHY" 50
F1 "sch/usb_phy.sch" 50
$EndSheet
$Sheet
S 8450 2450 1000 750 
U 5FFD078E
F0 "STBY_PWR" 50
F1 "sch/stby_pwr.sch" 50
$EndSheet
$Sheet
S 4050 1800 1300 2300
U 5DE9CBEC
F0 "PL_MGT" 50
F1 "sch/pl_mgt.sch" 50
$EndSheet
$Sheet
S 8450 3400 1000 750 
U 5F7AC3E6
F0 "PYLD_PWR" 50
F1 "sch/pyld_pwr.sch" 50
$EndSheet
$Sheet
S 8450 1000 1000 550 
U 5EB76D90
F0 "Ethernet" 50
F1 "sch/ethernet.sch" 50
$EndSheet
$Comp
L KIT_Graphic:BZB_LOGO G3
U 1 1 5F61B767
P 10850 6850
F 0 "G3" H 10850 6634 60  0001 C CNN
F 1 "BZB_LOGO" H 10850 7066 60  0001 C CNN
F 2 "KIT_Graphic:BZB" H 10850 6850 50  0001 C CNN
F 3 "" H 10850 6850 50  0001 C CNN
	1    10850 6850
	1    0    0    -1  
$EndComp
$Sheet
S 7000 4350 1100 1100
U 5EC80A2F
F0 "B504" 50
F1 "sch/b504.sch" 50
$EndSheet
$Sheet
S 4050 5050 1300 400 
U 5F2E039A
F0 "B65" 50
F1 "sch/b65.sch" 50
$EndSheet
$Sheet
S 4050 4350 1300 400 
U 5F2E03FC
F0 "B66" 50
F1 "sch/b66.sch" 50
$EndSheet
$Sheet
S 5550 4350 1250 1100
U 5F3D10BF
F0 "B64" 50
F1 "sch/b64.sch" 50
$EndSheet
$Sheet
S 5550 1800 500  700 
U 5F4D38EE
F0 "B46" 50
F1 "sch/b46.sch" 50
$EndSheet
$Sheet
S 6300 1800 500  700 
U 5F4D391C
F0 "B45" 50
F1 "sch/b45.sch" 50
$EndSheet
$Sheet
S 950  4500 1100 450 
U 5EF3A6E7
F0 "AB" 50
F1 "sch/fmcp_ab.sch" 50
$EndSheet
$Sheet
S 950  3850 1100 450 
U 5EF3A6E9
F0 "CD" 50
F1 "sch/fmcp_cd.sch" 50
$EndSheet
$Sheet
S 950  3200 1100 450 
U 5EF3A6EB
F0 "EF" 50
F1 "sch/fmcp_ef.sch" 50
$EndSheet
$Sheet
S 950  2550 1100 450 
U 5EF3A6ED
F0 "GH" 50
F1 "sch/fmcp_gh.sch" 50
$EndSheet
$Sheet
S 950  1900 1100 450 
U 5EF3A6EF
F0 "JK" 50
F1 "sch/fmcp_jk.sch" 50
$EndSheet
$Sheet
S 950  1250 1100 450 
U 5EF3A6F1
F0 "LM" 50
F1 "sch/fmcp_lm.sch" 50
$EndSheet
$Sheet
S 950  5150 1100 450 
U 5EF3A6F3
F0 "YZ" 50
F1 "sch/fmcp_yz.sch" 50
$EndSheet
Text Notes 1350 1550 0    50   ~ 0
DP\nREFCLK
Text Notes 1450 2200 2    50   ~ 0
HA\nHB
Text Notes 1450 2800 2    50   ~ 0
LA
Text Notes 1450 3450 2    50   ~ 0
HA\nHB
Text Notes 1450 4100 2    50   ~ 0
LA
Text Notes 1450 4750 2    50   ~ 0
DP
Text Notes 1450 5400 2    50   ~ 0
DP
$Sheet
S 6300 1000 500  550 
U 5F0BF924
F0 "MISC" 50
F1 "sch/misc.sch" 50
$EndSheet
$Comp
L power:GND #PWR?
U 1 1 5FD866F3
P 6250 7500
F 0 "#PWR?" H 6250 7250 50  0001 C CNN
F 1 "GND" H 6255 7327 50  0000 C CNN
F 2 "" H 6250 7500 50  0001 C CNN
F 3 "" H 6250 7500 50  0001 C CNN
	1    6250 7500
	1    0    0    -1  
$EndComp
$Comp
L KIT_Mechanical:TestPoint_0603 TP3
U 1 1 5FD99E1D
P 6400 7200
F 0 "TP3" H 6660 7294 50  0001 L CNN
F 1 "GND" H 6660 7248 50  0001 L CNN
F 2 "KIT_Mechanical:TP_0603_1608Metric" H 6600 7200 50  0001 C CNN
F 3 "http://www.koaspeer.com/pdfs/RC.pdf" H 6600 7200 50  0001 C CNN
F 4 "2019-RCUCTECT-ND" H 6400 7200 50  0001 C CNN "digikey#"
F 5 "PC Test Point ,  Plating Surface Mount Mounting Type" H 6400 7200 50  0001 C CNN "info"
F 6 "KOA Speer Electronics, Inc." H 6400 7200 50  0001 C CNN "manf"
F 7 "RCUCTE" H 6400 7200 50  0001 C CNN "manf#"
	1    6400 7200
	1    0    0    -1  
$EndComp
$Comp
L KIT_Mechanical:TestPoint_0603 TP4
U 1 1 5FD99F8F
P 6400 7300
F 0 "TP4" H 6660 7394 50  0001 L CNN
F 1 "GND" H 6660 7348 50  0001 L CNN
F 2 "KIT_Mechanical:TP_0603_1608Metric" H 6600 7300 50  0001 C CNN
F 3 "http://www.koaspeer.com/pdfs/RC.pdf" H 6600 7300 50  0001 C CNN
F 4 "2019-RCUCTECT-ND" H 6400 7300 50  0001 C CNN "digikey#"
F 5 "PC Test Point ,  Plating Surface Mount Mounting Type" H 6400 7300 50  0001 C CNN "info"
F 6 "KOA Speer Electronics, Inc." H 6400 7300 50  0001 C CNN "manf"
F 7 "RCUCTE" H 6400 7300 50  0001 C CNN "manf#"
	1    6400 7300
	1    0    0    -1  
$EndComp
$Comp
L KIT_Mechanical:TestPoint_0603 TP5
U 1 1 5FD9A093
P 6400 7400
F 0 "TP5" H 6660 7494 50  0001 L CNN
F 1 "GND" H 6660 7448 50  0001 L CNN
F 2 "KIT_Mechanical:TP_0603_1608Metric" H 6600 7400 50  0001 C CNN
F 3 "http://www.koaspeer.com/pdfs/RC.pdf" H 6600 7400 50  0001 C CNN
F 4 "2019-RCUCTECT-ND" H 6400 7400 50  0001 C CNN "digikey#"
F 5 "PC Test Point ,  Plating Surface Mount Mounting Type" H 6400 7400 50  0001 C CNN "info"
F 6 "KOA Speer Electronics, Inc." H 6400 7400 50  0001 C CNN "manf"
F 7 "RCUCTE" H 6400 7400 50  0001 C CNN "manf#"
	1    6400 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 7100 6250 7100
Wire Wire Line
	6250 7100 6250 7200
Wire Wire Line
	6400 7200 6250 7200
Connection ~ 6250 7200
Wire Wire Line
	6250 7200 6250 7300
Wire Wire Line
	6400 7300 6250 7300
Connection ~ 6250 7300
Wire Wire Line
	6250 7300 6250 7400
Wire Wire Line
	6400 7400 6250 7400
Connection ~ 6250 7400
Wire Wire Line
	6250 7400 6250 7500
Text Notes 6000 6800 0    98   ~ 20
TestPoints
$Comp
L KIT_Mechanical:TestPoint_0603 TP2
U 1 1 5FD85E04
P 6400 7100
F 0 "TP2" H 6660 7194 50  0001 L CNN
F 1 "GND" H 6660 7148 50  0001 L CNN
F 2 "KIT_Mechanical:TP_0603_1608Metric" H 6600 7100 50  0001 C CNN
F 3 "http://www.koaspeer.com/pdfs/RC.pdf" H 6600 7100 50  0001 C CNN
F 4 "2019-RCUCTECT-ND" H 6400 7100 50  0001 C CNN "digikey#"
F 5 "RCUCTE" H 6400 7100 50  0001 C CNN "manf#"
F 6 "KOA Speer Electronics, Inc." H 6400 7100 50  0001 C CNN "manf"
F 7 "PC Test Point ,  Plating Surface Mount Mounting Type" H 6400 7100 50  0001 C CNN "info"
	1    6400 7100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
