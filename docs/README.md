# ZynqMP Mezzanine FMC+ Documetation 

## Testing and Configuring a newly built board

1. Make sure all power rails are not in short by measuring the resistance to GND and comparing against this values

 Board #1  

| Voltage           | Resistance [ohm]  |
|-------------------|-------------------|
| +3V3_STBY         | 12k               |
| +2V5_STBY         | 537               |
| +1V2_STBY         | 1.2k              |     
| +0.85V_STBY       | 7.5               |
| +1V8_STBY         | 773               |
| +1V2_PS_PLL       | 2.1k              |
| +5V_STBY          | 27.5k             |
| +DDR_VTT          | 13k               |
| +DDR_VREFA        | 283k              |
| +12V              | O.L.              |
| +0.85V_PL_VCCINT  | 1.7               |
| +5V               | 267               |
| +1V2_PL_MGTAVTT   | 3.3k              |
| +0.9V_PL_MGTAVCC  | 57.2              |
| +1V8_PL_MGTVCCAUX | 3.6k              |

2. Make sure the board has all ECOs implemented

- Resistor divider for FB on +2V5_STBY
- Resistor divider for FB on +5V
- 10uF on pin one of U23
- Pin 22 of U43 needs to be 5V instead of 12V

Optional:

- add wire from Pin 1 of U42 to 3.3V to enable output and test power supply before I2C I/O expander driver is available

3. Put jumper on J7 to disable U29 outputs, and connect Infineon programmer

4. Connect 3V3_STBY to external power supply, consider limiting the current to 400mA, when turned ON, you should see a 
current of about 208 mA when unconfigured

5. Configure U29 and U43 using Infineon IR PowIRCenter according to the following images, increase the current limit to 
1A @ 3.3V and enable the output, you should see a current consumtion of about 743mA @ 3.3V and 72.8mA @ 12V

6. Measure that all voltages are correct with a multimeter

7. when the Mezzanine is plugged to the main board and configured with a simple "hello world" app you get 178mA @ 48V 
and using the pmbus gui we see 2A @ 0.85V_STBY

8. run simple application via JTAG connecting the serial interface in connector ST149 to a raspberry pi using bootmode 0x0

```
sudo minicom -b 115200 -o -D /dev/serial0
```

9. test simple application via SDcard using bootmode 0xE, clock frequency for SD is 20MHz @ 3V with a kingston 8GB speed 4 card

