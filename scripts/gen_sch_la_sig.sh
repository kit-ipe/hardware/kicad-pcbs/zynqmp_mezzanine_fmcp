#!/bin/bash

#Text Label 4650 6750 0    50   ~ 0
#PL_MGT_TX0_N


for i in {0..16}
do
  echo "Text Label 17550 $((2950 + i * 200)) 0    50   ~ 0"
  echo "FMCP_M2C_LA_${i}_N"
done

for i in {0..16}
do
  echo "Text Label 17550 $((3050 + i * 200)) 0    50   ~ 0"
  echo "FMCP_M2C_LA_${i}_P"
done
for i in {17..33}
do
  echo "Text Label 10600 $((2950 + i * 200)) 0    50   ~ 0"
  echo "FMCP_C2M_LA_${i}_N"
done
for i in {17..33}
do
  echo "Text Label 10600 $((3050 + i * 200)) 0    50   ~ 0"
  echo "FMCP_C2M_LA_${i}_P"
done
