#!/bin/bash

#Text Label 4650 6750 0    50   ~ 0
#PL_MGT_TX0_N


for i in {0..23}
do
  echo "Text GLabel 3200 $((9500 + i * 200)) 0    50  Input ~ 0"
  echo "PL_MGT_CLK_N${i}"
done

for i in {0..23}
do
  echo "Text GLabel 3200 $((9600 + i * 200)) 0    50  Input ~ 0"
  echo "PL_MGT_CLK_P${i}"
done
for i in {0..23}
do
  echo "Text GLabel 5250 $((9500 + i * 200)) 0    50  Input ~ 0"
  echo "PS_MGT_CLK_N${i}"
done
for i in {0..23}
do
  echo "Text GLabel 5250 $((9600 + i * 200)) 0    50  Input ~ 0"
  echo "PS_MGT_CLK_P${i}"
done

