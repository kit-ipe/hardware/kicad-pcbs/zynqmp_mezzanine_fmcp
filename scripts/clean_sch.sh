#!/bin/bash
for file in ../sch/*.sch
do 
sed -i 's/Info/info/g' $file
sed -i 's/Digi-Key/digikey#/g' $file
sed -i 's/Mouser/mouser#/g' $file
sed -i 's/RS Components/rs#/g' $file

sed -i 's/Manufacturer/manf/g' $file

sed -i 's/MPN/manf#/g' $file

sed -i 's/Vendor/stock/g' $file

sed -i 's/Voltage/info/g' $file
sed -i 's/Tolerance/info/g' $file

sed -i 's/Supplier/stock/g' $file




done


