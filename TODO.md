# ZynqMP Mezzanine FMC+ TODO

- [ ] Library
    - [x] IRPS5401MTRPBF footprint: pads with no name connecting multiple pins
    - [x] check if DDR pads can be smaller than 0.42, maybe 0.35 and keep via 0.45/0.2 --> keep pads at 0.42 
    - [x] UMPT-04-01.5-T-VT-SM-WT-K
    - [x] UMPS-04-03.5-T-VT-SM-WT-K
    - [x] MAX16025TE+

- [ ] Schematics (ERC test)
    - [ ] power 
        - [x] write down power consumption per domain in a table 
        - [x] PS power complete enable and pg, slow control too and ferrites
        - [x] PL power consider adding diodes or FET to enable VCCINT from PS
        - [x] aggregate capacitor values 
        - [x] Use Si7157DP with potentially ltc4414 and LT1617/LT1617-1 for powering PL_VCCINT from PS_VCCINT? --> TPS22990N
    - [ ] config
        - [x] power-on-reset definition
        - [x] boot and conf signals at 3v3 check 
        - [x] add MCP9902/3/4 for DP-DN temp measurement
    - [ ] PL MGT 
        - [x] assign nets according to FMC+ connector to ease the routing
    - [ ] PS MGT
        - [x] add sel and PD signal to high-speed switch -> switch removed
    - [ ] Clocks
        - [x] assign clock nets according to receiving pin
        - [x] add clock to the FPGA side
        - [x] add slow control 
    - [ ] DDR
        - [x] assign address signals to resistor arrays for termination instead of singel resistors
    - [x] I2C
        - [x] PMBUS signals are pulled up in FMCP_JK sheet (3V3) -> connect to global net as well in MIO pins (1V8) (TCA9803) --> TWO SEPARATE PMBUS, INTERNAL MIO and EXTERNAL FPGA
        - [x] Check I2C addresses
        - [x] conect SI5395 control/status signals to IO expander to MIO? or to PL --> PL, the PL-MGTs are only active when Payload pwr is enabled. clks could be on but monitoring only via I2C
        
- [x] nice to have
    - [x] eMMC module on board -> not possible, not enough MIOs
    - [x] USB type C
    
- [ ] Layout
    - [x] identify core to be placed in the intersection of CMX and FMC+ designs
    - [x] place all components inside

- [ ] Routing
    - [x] Stack-up definition
    - [x] add 2 layers to make high-speed signals and clks enclosed between GND solid planes
    - [x] differential pairs geometry definition
    - [x] trace width for single ended 50 ohm in DDR
    - [x] Select specific IO in every FPGA bank according to best routing
    - [x] route DDR
        - [x] tune DDR
    - [x] route MGT
        - [ ] de-skew pairs
    - [x] route clocks 
        - [ ] de-skew pairs
    - [x] route power
    - [x] route eth
  
- [ ] Documentation
    - [ ] make html interactive block diagram of the system using Graphviz
        - [MNT Reform 2.0D-4 System Diagram](https://mntre.com/reform2-handbook/system.html)
        - [Doteditor](http://vincenthee.github.io/DotEditor/)
        - [Graphviz (dot) examples](https://renenyffenegger.ch/notes/tools/Graphviz/examples/index)
        - [VSCode support](https://marketplace.visualstudio.com/items?itemName=joaompinto.vscode-graphviz)
        - [Online Visual Editor](ttp://magjac.com/graphviz-visual-editor/)
        - [Sketchviz](https://sketchviz.com/new)
        - [XDot](https://github.com/jrfonseca/xdot.py)
    - [ ] HTML Interactive BOM and Trace Explorer 
    
