#!/bin/bash

sed -i 's/C_0201_0603Metric/0201_C/g' $1
sed -i 's/C_0402_1005Metric/0402_C/g' $1
sed -i 's/C_0603_1608Metric/0603_C/g' $1
sed -i 's/C_0805_2012Metric/0805_C/g' $1
sed -i 's/C_1206_3216Metric/1206_C/g' $1
sed -i 's/C_1210_3225Metric/1210_C/g' $1
sed -i 's/R_0201_0603Metric/0201_R/g' $1
sed -i 's/R_0402_1005Metric/0402_R/g' $1
sed -i 's/R_0603_1608Metric/0603_R/g' $1
sed -i 's/R_0805_2012Metric/0805_R/g' $1
sed -i 's/R_2512_6332Metric/2512_R/g' $1
sed -i 's/R_1206_3216Metric/1206_R/g' $1
sed -i 's/L_0201_0603Metric/0201_L/g' $1
sed -i 's/L_0402_1005Metric/0402_L/g' $1
sed -i 's/L_0603_1608Metric/0603_L/g' $1
sed -i 's/L_0805_2012Metric/0805_L/g' $1
sed -i 's/L_1008_2520Metric/1008_L/g' $1
sed -i 's/L_1206_3216Metric/1206_L/g' $1
sed -i 's/LED_0201_0603Metric/0201_LED/g' $1
sed -i 's/LED_0402_1005Metric/0402_LED/g' $1
sed -i 's/LED_0603_1608Metric/0603_LED/g' $1
sed -i 's/LED_0805_2012Metric/0805_LED/g' $1
sed -i 's/LED_1206_3216Metric/1206_LED/g' $1
sed -i 's/D_0402_1005Metric/0402_D/g' $1

