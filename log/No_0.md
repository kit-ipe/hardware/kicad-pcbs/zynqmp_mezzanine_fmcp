 # Mezzanine No 0

## 2021-10-15
Problem description:
- This board has suddenly stopped working. No boot was possible.

Observations/Investigations:
- 1V8_STBY has dropped to 0.45V @ 11A (Measured using the PMBus interface of U29).
- Physically touching L6 "fixed" the problem. Thus, a possible cause could be a bad solder joint of L6.
