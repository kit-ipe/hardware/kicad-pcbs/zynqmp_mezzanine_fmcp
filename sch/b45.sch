EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 20 28
Title "ZynqMP Mezzanine FMC+ (BuZZyBoard)"
Date "2020-10-02"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "Institute for Data Processing and Electronics (IPE)"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 2 1 5E4884CB
P 5100 3000
F 0 "U1" H 6000 3290 60  0000 C CNN
F 1 "ZU4EG-B900" H 6000 3184 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 3400 2800 50  0001 C CNN
F 3 "" H 3400 2800 50  0001 C CNN
	2    5100 3000
	1    0    0    -1  
$EndComp
Text Label 4150 3100 0    50   ~ 0
B45_IO_0
Text Label 4150 3200 0    50   ~ 0
B45_IO_1
Text Label 4150 3300 0    50   ~ 0
B45_IO_2
Text Label 4150 3500 0    50   ~ 0
B45_IO_4
Text Label 4150 3600 0    50   ~ 0
B45_IO_5
Text Label 4150 3700 0    50   ~ 0
B45_IO_6
Text Label 4150 3800 0    50   ~ 0
B45_IO_7
Text Label 4150 3900 0    50   ~ 0
B45_IO_8
Text Label 4150 4000 0    50   ~ 0
B45_IO_9
Text Label 4150 4100 0    50   ~ 0
B45_IO_10
Text Label 4150 4200 0    50   ~ 0
B45_IO_11
Text Label 7850 3100 2    50   ~ 0
B45_IO_12
Text Label 7850 3200 2    50   ~ 0
B45_IO_13
Text Label 7850 3300 2    50   ~ 0
B45_IO_14
Text Label 7850 3400 2    50   ~ 0
B45_IO_15
Text Label 7850 3500 2    50   ~ 0
B45_IO_16
Text Label 7850 3600 2    50   ~ 0
B45_IO_17
Text Label 7850 3700 2    50   ~ 0
B45_IO_18
Text Label 7850 3800 2    50   ~ 0
B45_IO_19
Text Label 7850 3900 2    50   ~ 0
B45_IO_20
Text Label 7850 4000 2    50   ~ 0
B45_IO_21
Text Label 7850 4100 2    50   ~ 0
B45_IO_22
Text Label 7850 4200 2    50   ~ 0
B45_IO_23
Wire Wire Line
	4150 3100 4700 3100
Wire Wire Line
	4150 3200 4700 3200
Wire Wire Line
	4150 3300 4700 3300
Wire Wire Line
	4150 3400 4700 3400
Wire Wire Line
	4150 3500 4700 3500
Wire Wire Line
	4150 3600 4700 3600
Wire Wire Line
	4150 3700 4700 3700
Wire Wire Line
	4150 3800 4700 3800
Wire Wire Line
	4150 3900 4700 3900
Wire Wire Line
	4150 4000 4700 4000
Wire Wire Line
	4150 4100 4700 4100
Wire Wire Line
	4150 4200 4700 4200
Wire Wire Line
	7850 3100 7300 3100
Wire Wire Line
	7850 3200 7300 3200
Wire Wire Line
	7850 3300 7300 3300
Wire Wire Line
	7850 3400 7300 3400
Wire Wire Line
	7850 3500 7300 3500
Wire Wire Line
	7850 3600 7300 3600
Wire Wire Line
	7850 3700 7300 3700
Wire Wire Line
	7850 3800 7300 3800
Wire Wire Line
	7850 3900 7300 3900
Wire Wire Line
	7850 4000 7300 4000
Wire Wire Line
	7850 4100 7300 4100
Wire Wire Line
	7850 4200 7300 4200
Entry Wire Line
	7850 3100 7950 3200
Entry Wire Line
	7850 3200 7950 3300
Entry Wire Line
	7850 3300 7950 3400
Entry Wire Line
	7850 3400 7950 3500
Entry Wire Line
	7850 3500 7950 3600
Entry Wire Line
	7850 3600 7950 3700
Entry Wire Line
	7850 3700 7950 3800
Entry Wire Line
	7850 3800 7950 3900
Entry Wire Line
	7850 3900 7950 4000
Entry Wire Line
	7850 4000 7950 4100
Entry Wire Line
	7850 4100 7950 4200
Entry Wire Line
	7850 4200 7950 4300
Entry Wire Line
	4150 3100 4050 3200
Entry Wire Line
	4150 3500 4050 3600
Entry Wire Line
	4150 3700 4050 3800
Entry Wire Line
	4150 3800 4050 3900
Entry Wire Line
	4150 3900 4050 4000
Entry Wire Line
	4150 4000 4050 4100
Entry Wire Line
	4150 4100 4050 4200
Entry Wire Line
	4150 4200 4050 4300
Wire Bus Line
	7950 4600 4050 4600
Connection ~ 4050 4600
Wire Bus Line
	4050 4600 3500 4600
Text GLabel 3500 4600 0    50   BiDi ~ 0
B45_IO_[0..23]
Wire Wire Line
	4450 3000 4700 3000
Wire Wire Line
	4450 2350 4450 3000
$Comp
L KIT_Power:+3V3_STBY #PWR?
U 1 1 613DD271
P 3400 2300
F 0 "#PWR?" H 3400 2150 50  0001 C CNN
F 1 "+3V3_STBY" H 3415 2473 50  0000 C CNN
F 2 "" H 3400 2300 50  0001 C CNN
F 3 "" H 3400 2300 50  0001 C CNN
	1    3400 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2350 3850 2350
Wire Wire Line
	3400 2400 3400 2350
Wire Wire Line
	3400 2750 3400 2700
$Comp
L power:GND #PWR?
U 1 1 5FF351AC
P 3400 2750
F 0 "#PWR?" H 3400 2500 50  0001 C CNN
F 1 "GND" H 3405 2577 50  0000 C CNN
F 2 "" H 3400 2750 50  0001 C CNN
F 3 "" H 3400 2750 50  0001 C CNN
	1    3400 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C60
U 1 1 5FF351A6
P 3400 2550
F 0 "C60" H 3515 2596 50  0000 L CNN
F 1 "0.1uF" H 3515 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3438 2400 50  0001 C CNN
F 3 "~" H 3400 2550 50  0001 C CNN
F 4 "16 V" H 3400 2550 50  0001 C CNN "info"
F 5 "AVT-IPE" H 3400 2550 50  0001 C CNN "stock"
	1    3400 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2400 3850 2350
Wire Wire Line
	3850 2750 3850 2700
$Comp
L power:GND #PWR?
U 1 1 5FF3519D
P 3850 2750
F 0 "#PWR?" H 3850 2500 50  0001 C CNN
F 1 "GND" H 3855 2577 50  0000 C CNN
F 2 "" H 3850 2750 50  0001 C CNN
F 3 "" H 3850 2750 50  0001 C CNN
	1    3850 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C62
U 1 1 5FF35197
P 3850 2550
F 0 "C62" H 3965 2596 50  0000 L CNN
F 1 "0.1uF" H 3965 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3888 2400 50  0001 C CNN
F 3 "~" H 3850 2550 50  0001 C CNN
F 4 "16 V" H 3850 2550 50  0001 C CNN "info"
F 5 "AVT-IPE" H 3850 2550 50  0001 C CNN "stock"
	1    3850 2550
	1    0    0    -1  
$EndComp
Connection ~ 3850 2350
Wire Wire Line
	3850 2350 4450 2350
Connection ~ 3400 2350
Wire Wire Line
	3400 2300 3400 2350
Text Notes 8450 3300 0    50   ~ 0
GC\npins
Wire Notes Line
	6350 3450 8650 3450
Wire Notes Line
	8650 3450 8650 3050
Wire Notes Line
	8650 3050 6350 3050
Text Notes 3600 4100 2    50   ~ 0
GC\npins
Wire Notes Line
	5700 4250 3400 4250
Wire Notes Line
	3400 4250 3400 3850
Wire Notes Line
	3400 3850 5700 3850
Wire Notes Line
	5700 4250 5700 3850
Wire Notes Line
	6350 3450 6350 3050
Text Label 4150 3400 0    50   ~ 0
B45_IO_3
NoConn ~ 4150 3300
NoConn ~ 4150 3400
NoConn ~ 4150 3200
NoConn ~ 4150 3600
Wire Bus Line
	4050 3200 4050 4600
Wire Bus Line
	7950 3200 7950 4600
$EndSCHEMATC
