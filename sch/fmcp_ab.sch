EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 21 28
Title "ZynqMP Mezzanine FMC+ (BuZZyBoard)"
Date "2020-10-02"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "Institute for Data Processing and Electronics (IPE)"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KIT_Connector:FMCP_VITA57.4 J2
U 1 1 5EE2AA7F
P 5200 2000
F 0 "J2" H 6100 2290 60  0000 C CNN
F 1 "FMCP_VITA57.4" H 6100 2184 60  0000 C CNN
F 2 "KIT_Connector:FMC_SEAM_ASP_184330_BGA-560_40x14_55.778x19.76mm" H 5100 1750 60  0001 L CNN
F 3 "http://suddendocs.samtec.com/prints/asp-188588-01-mkt.pdf" H 5100 1550 60  0001 L CNN
	1    5200 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0431BA
P 5200 2300
F 0 "#PWR?" H 5200 2050 50  0001 C CNN
F 1 "GND" V 5205 2172 50  0000 R CNN
F 2 "" H 5200 2300 50  0001 C CNN
F 3 "" H 5200 2300 50  0001 C CNN
	1    5200 2300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0431C0
P 5200 2400
F 0 "#PWR?" H 5200 2150 50  0001 C CNN
F 1 "GND" V 5205 2272 50  0000 R CNN
F 2 "" H 5200 2400 50  0001 C CNN
F 3 "" H 5200 2400 50  0001 C CNN
	1    5200 2400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0438EC
P 5200 2700
F 0 "#PWR?" H 5200 2450 50  0001 C CNN
F 1 "GND" V 5205 2572 50  0000 R CNN
F 2 "" H 5200 2700 50  0001 C CNN
F 3 "" H 5200 2700 50  0001 C CNN
	1    5200 2700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0438F2
P 5200 2800
F 0 "#PWR?" H 5200 2550 50  0001 C CNN
F 1 "GND" V 5205 2672 50  0000 R CNN
F 2 "" H 5200 2800 50  0001 C CNN
F 3 "" H 5200 2800 50  0001 C CNN
	1    5200 2800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F043FFC
P 5200 3100
F 0 "#PWR?" H 5200 2850 50  0001 C CNN
F 1 "GND" V 5205 2972 50  0000 R CNN
F 2 "" H 5200 3100 50  0001 C CNN
F 3 "" H 5200 3100 50  0001 C CNN
	1    5200 3100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F044002
P 5200 3200
F 0 "#PWR?" H 5200 2950 50  0001 C CNN
F 1 "GND" V 5205 3072 50  0000 R CNN
F 2 "" H 5200 3200 50  0001 C CNN
F 3 "" H 5200 3200 50  0001 C CNN
	1    5200 3200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04E6DC
P 5200 3500
F 0 "#PWR?" H 5200 3250 50  0001 C CNN
F 1 "GND" V 5205 3372 50  0000 R CNN
F 2 "" H 5200 3500 50  0001 C CNN
F 3 "" H 5200 3500 50  0001 C CNN
	1    5200 3500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04E6E2
P 5200 3600
F 0 "#PWR?" H 5200 3350 50  0001 C CNN
F 1 "GND" V 5205 3472 50  0000 R CNN
F 2 "" H 5200 3600 50  0001 C CNN
F 3 "" H 5200 3600 50  0001 C CNN
	1    5200 3600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04F13A
P 5200 3900
F 0 "#PWR?" H 5200 3650 50  0001 C CNN
F 1 "GND" V 5205 3772 50  0000 R CNN
F 2 "" H 5200 3900 50  0001 C CNN
F 3 "" H 5200 3900 50  0001 C CNN
	1    5200 3900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04F140
P 5200 4000
F 0 "#PWR?" H 5200 3750 50  0001 C CNN
F 1 "GND" V 5205 3872 50  0000 R CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "" H 5200 4000 50  0001 C CNN
	1    5200 4000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04F73A
P 5200 4300
F 0 "#PWR?" H 5200 4050 50  0001 C CNN
F 1 "GND" V 5205 4172 50  0000 R CNN
F 2 "" H 5200 4300 50  0001 C CNN
F 3 "" H 5200 4300 50  0001 C CNN
	1    5200 4300
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F04F740
P 5200 4400
F 0 "#PWR?" H 5200 4150 50  0001 C CNN
F 1 "GND" V 5205 4272 50  0000 R CNN
F 2 "" H 5200 4400 50  0001 C CNN
F 3 "" H 5200 4400 50  0001 C CNN
	1    5200 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F051350
P 5200 4700
F 0 "#PWR?" H 5200 4450 50  0001 C CNN
F 1 "GND" V 5205 4572 50  0000 R CNN
F 2 "" H 5200 4700 50  0001 C CNN
F 3 "" H 5200 4700 50  0001 C CNN
	1    5200 4700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F051356
P 5200 4800
F 0 "#PWR?" H 5200 4550 50  0001 C CNN
F 1 "GND" V 5205 4672 50  0000 R CNN
F 2 "" H 5200 4800 50  0001 C CNN
F 3 "" H 5200 4800 50  0001 C CNN
	1    5200 4800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F051A1A
P 5200 5100
F 0 "#PWR?" H 5200 4850 50  0001 C CNN
F 1 "GND" V 5205 4972 50  0000 R CNN
F 2 "" H 5200 5100 50  0001 C CNN
F 3 "" H 5200 5100 50  0001 C CNN
	1    5200 5100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F051A20
P 5200 5200
F 0 "#PWR?" H 5200 4950 50  0001 C CNN
F 1 "GND" V 5205 5072 50  0000 R CNN
F 2 "" H 5200 5200 50  0001 C CNN
F 3 "" H 5200 5200 50  0001 C CNN
	1    5200 5200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F052224
P 5200 5500
F 0 "#PWR?" H 5200 5250 50  0001 C CNN
F 1 "GND" V 5205 5372 50  0000 R CNN
F 2 "" H 5200 5500 50  0001 C CNN
F 3 "" H 5200 5500 50  0001 C CNN
	1    5200 5500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05222A
P 5200 5600
F 0 "#PWR?" H 5200 5350 50  0001 C CNN
F 1 "GND" V 5205 5472 50  0000 R CNN
F 2 "" H 5200 5600 50  0001 C CNN
F 3 "" H 5200 5600 50  0001 C CNN
	1    5200 5600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0529DE
P 5200 5900
F 0 "#PWR?" H 5200 5650 50  0001 C CNN
F 1 "GND" V 5205 5772 50  0000 R CNN
F 2 "" H 5200 5900 50  0001 C CNN
F 3 "" H 5200 5900 50  0001 C CNN
	1    5200 5900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F054CAC
P 7000 5700
F 0 "#PWR?" H 7000 5450 50  0001 C CNN
F 1 "GND" V 7005 5572 50  0000 R CNN
F 2 "" H 7000 5700 50  0001 C CNN
F 3 "" H 7000 5700 50  0001 C CNN
	1    7000 5700
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F054CB2
P 7000 5800
F 0 "#PWR?" H 7000 5550 50  0001 C CNN
F 1 "GND" V 7005 5672 50  0000 R CNN
F 2 "" H 7000 5800 50  0001 C CNN
F 3 "" H 7000 5800 50  0001 C CNN
	1    7000 5800
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0554CE
P 7000 5300
F 0 "#PWR?" H 7000 5050 50  0001 C CNN
F 1 "GND" V 7005 5172 50  0000 R CNN
F 2 "" H 7000 5300 50  0001 C CNN
F 3 "" H 7000 5300 50  0001 C CNN
	1    7000 5300
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F0554D4
P 7000 5400
F 0 "#PWR?" H 7000 5150 50  0001 C CNN
F 1 "GND" V 7005 5272 50  0000 R CNN
F 2 "" H 7000 5400 50  0001 C CNN
F 3 "" H 7000 5400 50  0001 C CNN
	1    7000 5400
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F055A6A
P 7000 4900
F 0 "#PWR?" H 7000 4650 50  0001 C CNN
F 1 "GND" V 7005 4772 50  0000 R CNN
F 2 "" H 7000 4900 50  0001 C CNN
F 3 "" H 7000 4900 50  0001 C CNN
	1    7000 4900
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F055A70
P 7000 5000
F 0 "#PWR?" H 7000 4750 50  0001 C CNN
F 1 "GND" V 7005 4872 50  0000 R CNN
F 2 "" H 7000 5000 50  0001 C CNN
F 3 "" H 7000 5000 50  0001 C CNN
	1    7000 5000
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F058588
P 7000 4500
F 0 "#PWR?" H 7000 4250 50  0001 C CNN
F 1 "GND" V 7005 4372 50  0000 R CNN
F 2 "" H 7000 4500 50  0001 C CNN
F 3 "" H 7000 4500 50  0001 C CNN
	1    7000 4500
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05858E
P 7000 4600
F 0 "#PWR?" H 7000 4350 50  0001 C CNN
F 1 "GND" V 7005 4472 50  0000 R CNN
F 2 "" H 7000 4600 50  0001 C CNN
F 3 "" H 7000 4600 50  0001 C CNN
	1    7000 4600
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F058C2A
P 7000 4100
F 0 "#PWR?" H 7000 3850 50  0001 C CNN
F 1 "GND" V 7005 3972 50  0000 R CNN
F 2 "" H 7000 4100 50  0001 C CNN
F 3 "" H 7000 4100 50  0001 C CNN
	1    7000 4100
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F058C30
P 7000 4200
F 0 "#PWR?" H 7000 3950 50  0001 C CNN
F 1 "GND" V 7005 4072 50  0000 R CNN
F 2 "" H 7000 4200 50  0001 C CNN
F 3 "" H 7000 4200 50  0001 C CNN
	1    7000 4200
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F059438
P 7000 3700
F 0 "#PWR?" H 7000 3450 50  0001 C CNN
F 1 "GND" V 7005 3572 50  0000 R CNN
F 2 "" H 7000 3700 50  0001 C CNN
F 3 "" H 7000 3700 50  0001 C CNN
	1    7000 3700
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05943E
P 7000 3800
F 0 "#PWR?" H 7000 3550 50  0001 C CNN
F 1 "GND" V 7005 3672 50  0000 R CNN
F 2 "" H 7000 3800 50  0001 C CNN
F 3 "" H 7000 3800 50  0001 C CNN
	1    7000 3800
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05C332
P 7000 3300
F 0 "#PWR?" H 7000 3050 50  0001 C CNN
F 1 "GND" V 7005 3172 50  0000 R CNN
F 2 "" H 7000 3300 50  0001 C CNN
F 3 "" H 7000 3300 50  0001 C CNN
	1    7000 3300
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05C338
P 7000 3400
F 0 "#PWR?" H 7000 3150 50  0001 C CNN
F 1 "GND" V 7005 3272 50  0000 R CNN
F 2 "" H 7000 3400 50  0001 C CNN
F 3 "" H 7000 3400 50  0001 C CNN
	1    7000 3400
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05CA90
P 7000 2900
F 0 "#PWR?" H 7000 2650 50  0001 C CNN
F 1 "GND" V 7005 2772 50  0000 R CNN
F 2 "" H 7000 2900 50  0001 C CNN
F 3 "" H 7000 2900 50  0001 C CNN
	1    7000 2900
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05CA96
P 7000 3000
F 0 "#PWR?" H 7000 2750 50  0001 C CNN
F 1 "GND" V 7005 2872 50  0000 R CNN
F 2 "" H 7000 3000 50  0001 C CNN
F 3 "" H 7000 3000 50  0001 C CNN
	1    7000 3000
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05D30E
P 7000 2500
F 0 "#PWR?" H 7000 2250 50  0001 C CNN
F 1 "GND" V 7005 2372 50  0000 R CNN
F 2 "" H 7000 2500 50  0001 C CNN
F 3 "" H 7000 2500 50  0001 C CNN
	1    7000 2500
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05D314
P 7000 2600
F 0 "#PWR?" H 7000 2350 50  0001 C CNN
F 1 "GND" V 7005 2472 50  0000 R CNN
F 2 "" H 7000 2600 50  0001 C CNN
F 3 "" H 7000 2600 50  0001 C CNN
	1    7000 2600
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05FDF4
P 7000 2100
F 0 "#PWR?" H 7000 1850 50  0001 C CNN
F 1 "GND" V 7005 1972 50  0000 R CNN
F 2 "" H 7000 2100 50  0001 C CNN
F 3 "" H 7000 2100 50  0001 C CNN
	1    7000 2100
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F05FDFA
P 7000 2200
F 0 "#PWR?" H 7000 1950 50  0001 C CNN
F 1 "GND" V 7005 2072 50  0000 R CNN
F 2 "" H 7000 2200 50  0001 C CNN
F 3 "" H 7000 2200 50  0001 C CNN
	1    7000 2200
	0    -1   1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FD6E40A
P 8500 1450
AR Path="/5EF3A6E9/5FD6E40A" Ref="R?"  Part="1" 
AR Path="/5EF3A6E7/5FD6E40A" Ref="R23"  Part="1" 
F 0 "R23" H 8570 1496 50  0000 L CNN
F 1 "10k" H 8570 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8430 1450 50  0001 C CNN
F 3 "~" H 8500 1450 50  0001 C CNN
F 4 "AVT-IPE" H 8500 1450 50  0001 C CNN "stock"
	1    8500 1450
	1    0    0    -1  
$EndComp
Text Notes 9400 1750 2    50   ~ 0
Direction of \nBIDIR CLKs 
$Comp
L KIT_Power:+3V3_STBY #PWR?
U 1 1 5FD6E412
P 8500 1300
AR Path="/5EF3A6E9/5FD6E412" Ref="#PWR?"  Part="1" 
AR Path="/5EF3A6E7/5FD6E412" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8500 1150 50  0001 C CNN
F 1 "+3V3_STBY" H 8515 1473 50  0000 C CNN
F 2 "" H 8500 1300 50  0001 C CNN
F 3 "" H 8500 1300 50  0001 C CNN
	1    8500 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FD6E419
P 8500 1850
AR Path="/5EF3A6E9/5FD6E419" Ref="R?"  Part="1" 
AR Path="/5EF3A6E7/5FD6E419" Ref="R24"  Part="1" 
F 0 "R24" H 8570 1941 50  0000 L CNN
F 1 "10k" H 8570 1850 50  0000 L CNN
F 2 "KIT_Resistor_SMD:R_0402_1005Metric_DNP" V 8430 1850 50  0001 C CNN
F 3 "~" H 8500 1850 50  0001 C CNN
F 4 "DNP" H 8570 1759 50  0000 L CNN "DNP"
F 5 "AVT-IPE" H 8500 1850 50  0001 C CNN "stock"
	1    8500 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD6E422
P 8500 2000
AR Path="/5EF3A6E9/5FD6E422" Ref="#PWR?"  Part="1" 
AR Path="/5EF3A6E7/5FD6E422" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8500 1750 50  0001 C CNN
F 1 "GND" H 8505 1827 50  0000 C CNN
F 2 "" H 8500 2000 50  0001 C CNN
F 3 "" H 8500 2000 50  0001 C CNN
	1    8500 2000
	1    0    0    -1  
$EndComp
Text Label 8100 1650 0    50   ~ 0
CLK_DIR
Wire Wire Line
	5200 2100 4800 2100
Wire Wire Line
	5200 2200 4800 2200
Wire Wire Line
	5200 2500 4800 2500
Wire Wire Line
	5200 2600 4800 2600
Wire Wire Line
	5200 2900 4800 2900
Wire Wire Line
	5200 3000 4800 3000
Wire Wire Line
	5200 3300 4800 3300
Wire Wire Line
	5200 3400 4800 3400
Wire Wire Line
	5200 3700 4800 3700
Wire Wire Line
	5200 3800 4800 3800
Wire Wire Line
	5200 4100 4800 4100
Wire Wire Line
	5200 4200 4800 4200
Wire Wire Line
	5200 4500 4800 4500
Wire Wire Line
	5200 4600 4800 4600
Wire Wire Line
	5200 4900 4800 4900
Wire Wire Line
	5200 5000 4800 5000
Wire Wire Line
	5200 5300 4800 5300
Wire Wire Line
	5200 5400 4800 5400
Wire Wire Line
	5200 5700 4800 5700
Wire Wire Line
	5200 5800 4800 5800
Text GLabel 4800 3300 0    50   Input ~ 0
PL_MGT_TX_P0
Text GLabel 4800 3700 0    50   Input ~ 0
PL_MGT_TX_P1
Text GLabel 7400 2700 2    50   Input ~ 0
PL_MGT_TX_P4
Text GLabel 4800 3400 0    50   Input ~ 0
PL_MGT_TX_N0
Text GLabel 4800 3800 0    50   Input ~ 0
PL_MGT_TX_N1
Text GLabel 7400 2800 2    50   Input ~ 0
PL_MGT_TX_N4
$Comp
L power:GND #PWR?
U 1 1 6213CEF2
P 5200 2000
F 0 "#PWR?" H 5200 1750 50  0001 C CNN
F 1 "GND" V 5205 1872 50  0000 R CNN
F 2 "" H 5200 2000 50  0001 C CNN
F 3 "" H 5200 2000 50  0001 C CNN
	1    5200 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 2300 7000 2300
Wire Wire Line
	7400 2400 7000 2400
Wire Wire Line
	7400 2700 7000 2700
Wire Wire Line
	7400 2800 7000 2800
Wire Wire Line
	7400 3100 7000 3100
Wire Wire Line
	7400 3200 7000 3200
Wire Wire Line
	7400 3500 7000 3500
Wire Wire Line
	7400 3600 7000 3600
Wire Wire Line
	7400 4000 7000 4000
Wire Wire Line
	7400 4300 7000 4300
Wire Wire Line
	7400 4400 7000 4400
Wire Wire Line
	7400 4700 7000 4700
Wire Wire Line
	7400 4800 7000 4800
Wire Wire Line
	7400 5100 7000 5100
Wire Wire Line
	7400 5200 7000 5200
Wire Wire Line
	7400 5500 7000 5500
Wire Wire Line
	7400 5600 7000 5600
Text GLabel 4800 2500 0    50   Input ~ 0
PS_MGT_TX_P2
Text GLabel 4800 2600 0    50   Input ~ 0
PS_MGT_TX_N2
Text GLabel 4800 4500 0    50   Output ~ 0
PS_MGT_RX_P2
Text GLabel 4800 4600 0    50   Output ~ 0
PS_MGT_RX_N2
Text Notes 4100 2550 2    50   ~ 0
SATA
Text Notes 4100 4550 2    50   ~ 0
SATA
Text Notes 4100 3350 2    50   ~ 0
C2C
Text Notes 4100 3750 2    50   ~ 0
C2C
Text Notes 4100 5350 2    50   ~ 0
C2C
Text Notes 4100 5750 2    50   ~ 0
C2C
Text Notes 8100 2750 0    50   ~ 0
TCDS2
Text Notes 8100 4750 0    50   ~ 0
TCDS2
Text GLabel 4800 2900 0    50   Input ~ 0
PS_MGT_TX_P3
Text GLabel 4800 3000 0    50   Input ~ 0
PS_MGT_TX_N3
Wire Wire Line
	7400 3900 7000 3900
NoConn ~ 4800 2100
NoConn ~ 4800 2200
NoConn ~ 4800 4100
NoConn ~ 4800 4200
Text GLabel 4800 4900 0    50   Output ~ 0
PS_MGT_RX_P3
Text GLabel 4800 5000 0    50   Output ~ 0
PS_MGT_RX_N3
Text Notes 4100 4950 2    50   ~ 0
SATA - Not connected in Motherboard!
Text Notes 4100 2950 2    50   ~ 0
SATA - Not connected in Motherboard!
NoConn ~ 7400 3900
NoConn ~ 7400 4000
Text Notes 4100 2200 2    50   ~ 0
USB3.0 type C
Text Notes 4100 4150 2    50   ~ 0
USB3.0 type C
Wire Wire Line
	7000 5900 7400 5900
NoConn ~ 7400 5900
Wire Notes Line
	8850 1850 9450 1850
Wire Notes Line
	9450 1850 9450 1500
Wire Notes Line
	9450 1500 8850 1500
Wire Notes Line
	8850 1500 8850 1850
Text GLabel 4800 5700 0    50   Input ~ 0
PL_MGT_RX_P1
Text GLabel 4800 5800 0    50   Input ~ 0
PL_MGT_RX_N1
Text GLabel 4800 5300 0    50   Input ~ 0
PL_MGT_RX_P0
Text GLabel 4800 5400 0    50   Input ~ 0
PL_MGT_RX_N0
Text GLabel 7400 4800 2    50   Output ~ 0
PL_MGT_RX_N4
Text GLabel 7400 4700 2    50   Output ~ 0
PL_MGT_RX_P4
Text GLabel 7400 4400 2    50   Output ~ 0
PL_MGT_RX_N5
Text GLabel 7400 4300 2    50   Output ~ 0
PL_MGT_RX_P5
Text Notes 8200 5150 0    50   ~ 0
Not connected in Motherboard!
Text Notes 8200 5550 0    50   ~ 0
Not connected in Motherboard!
Text Notes 8200 2350 0    50   ~ 0
Not connected in Motherboard!
Wire Wire Line
	8500 1600 8500 1650
Wire Wire Line
	8500 1650 7400 1650
Wire Wire Line
	7400 1650 7400 2000
Wire Wire Line
	7400 2000 7000 2000
Connection ~ 8500 1650
Wire Wire Line
	8500 1650 8500 1700
Text Notes 8200 3150 0    50   ~ 0
Not connected in Motherboard!
Text Notes 8200 3550 0    50   ~ 0
Not connected in Motherboard!
Text Notes 8200 3950 0    50   ~ 0
Not connected in Motherboard!
Text Notes 9600 1700 0    50   ~ 0
Not connected in Motherboard!
Text Notes 2150 2200 0    50   ~ 0
Not connected in Motherboard!
Text Notes 2200 4150 0    50   ~ 0
Not connected in Motherboard!
Text Notes 7500 2300 0    50   ~ 0
PL_MGT_TX_P5
Text Notes 7500 2400 0    50   ~ 0
PL_MGT_TX_N5
Text Notes 7500 3100 0    50   ~ 0
PL_MGT_TX_P3
Text Notes 7500 3200 0    50   ~ 0
PL_MGT_TX_N3
Text Notes 7500 3500 0    50   ~ 0
PL_MGT_TX_P2
Text Notes 7500 3600 0    50   ~ 0
PL_MGT_TX_N2
Text Notes 7500 5100 0    50   ~ 0
PL_MGT_RX_P3
Text Notes 7500 5200 0    50   ~ 0
PL_MGT_RX_N3
Text Notes 7500 5500 0    50   ~ 0
PL_MGT_RX_P2
Text Notes 7500 5600 0    50   ~ 0
PL_MGT_RX_N2
NoConn ~ 7400 2300
NoConn ~ 7400 2400
NoConn ~ 7400 3100
NoConn ~ 7400 3200
NoConn ~ 7400 3500
NoConn ~ 7400 3600
NoConn ~ 7400 5100
NoConn ~ 7400 5200
NoConn ~ 7400 5500
NoConn ~ 7400 5600
$EndSCHEMATC
