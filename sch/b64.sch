EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 18 28
Title "ZynqMP Mezzanine FMC+ (BuZZyBoard)"
Date "2020-10-02"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "Institute for Data Processing and Electronics (IPE)"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 4 1 5F3D439F
P 4800 2500
F 0 "U1" H 6200 2790 60  0000 C CNN
F 1 "ZU4EG-B900" H 6200 2684 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 3100 2300 50  0001 C CNN
F 3 "" H 3100 2300 50  0001 C CNN
	4    4800 2500
	1    0    0    -1  
$EndComp
Text Notes 3200 4800 2    50   ~ 0
GC\npins
Text Notes 9200 2850 2    50   ~ 0
GC\npins
NoConn ~ 8000 2500
Wire Wire Line
	1750 1850 1750 1900
Wire Wire Line
	2650 1900 3100 1900
$Comp
L Device:C C57
U 1 1 5F3D43A0
P 2650 2100
F 0 "C57" H 2765 2146 50  0000 L CNN
F 1 "0.1uF" H 2765 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2688 1950 50  0001 C CNN
F 3 "~" H 2650 2100 50  0001 C CNN
F 4 "16 V" H 2650 2100 50  0001 C CNN "info"
F 5 "AVT-IPE" H 2650 2100 50  0001 C CNN "stock"
	1    2650 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2300 2650 2250
Wire Wire Line
	2650 1950 2650 1900
Connection ~ 2650 1900
$Comp
L Device:C C56
U 1 1 5F3D43A2
P 2200 2100
F 0 "C56" H 2315 2146 50  0000 L CNN
F 1 "0.1uF" H 2315 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2238 1950 50  0001 C CNN
F 3 "~" H 2200 2100 50  0001 C CNN
F 4 "16 V" H 2200 2100 50  0001 C CNN "info"
F 5 "AVT-IPE" H 2200 2100 50  0001 C CNN "stock"
	1    2200 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1950 2200 1900
$Comp
L Device:C C58
U 1 1 5F3D43A3
P 3100 2100
F 0 "C58" H 3215 2146 50  0000 L CNN
F 1 "0.1uF" H 3215 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3138 1950 50  0001 C CNN
F 3 "~" H 3100 2100 50  0001 C CNN
F 4 "16 V" H 3100 2100 50  0001 C CNN "info"
F 5 "AVT-IPE" H 3100 2100 50  0001 C CNN "stock"
	1    3100 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2300 3100 2250
Wire Wire Line
	3100 1950 3100 1900
$Comp
L power:GND #PWR?
U 1 1 61A60C72
P 1750 2350
F 0 "#PWR?" H 1750 2100 50  0001 C CNN
F 1 "GND" H 1755 2177 50  0000 C CNN
F 2 "" H 1750 2350 50  0001 C CNN
F 3 "" H 1750 2350 50  0001 C CNN
	1    1750 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2300 2200 2250
Connection ~ 2200 1900
Wire Wire Line
	2200 1900 2650 1900
$Comp
L KIT_Power:+1V8_STBY #PWR?
U 1 1 619DC587
P 1750 1850
F 0 "#PWR?" H 1750 1700 50  0001 C CNN
F 1 "+1V8_STBY" H 1765 2023 50  0000 C CNN
F 2 "" H 1750 1850 50  0001 C CNN
F 3 "" H 1750 1850 50  0001 C CNN
	1    1750 1850
	1    0    0    -1  
$EndComp
Wire Notes Line
	6550 2550 9300 2550
Wire Notes Line
	9300 2550 9300 2950
Wire Notes Line
	9300 2950 6550 2950
Wire Notes Line
	6550 2950 6550 2550
Wire Wire Line
	3100 1900 3850 1900
Wire Wire Line
	3850 1900 3850 2500
Wire Wire Line
	3850 2500 4400 2500
Connection ~ 3100 1900
Wire Notes Line
	5750 4550 5750 4950
Wire Notes Line
	5750 4550 3000 4550
Wire Notes Line
	3000 4950 3000 4550
Wire Notes Line
	3000 4950 5750 4950
Text Label 3800 2600 0    50   ~ 0
B64_IO_0
Text Label 3800 2700 0    50   ~ 0
B64_IO_1
Text Label 3800 2800 0    50   ~ 0
B64_IO_2
Text Label 3800 2900 0    50   ~ 0
B64_IO_3
Text Label 3800 3000 0    50   ~ 0
B64_IO_4
Text Label 3800 3100 0    50   ~ 0
B64_IO_5
Text Label 3800 3200 0    50   ~ 0
B64_IO_6
Text Label 3800 3300 0    50   ~ 0
B64_IO_7
Text Label 3800 3400 0    50   ~ 0
B64_IO_8
Text Label 3800 3500 0    50   ~ 0
B64_IO_9
Text Label 3800 3600 0    50   ~ 0
B64_IO_10
Text Label 3800 3700 0    50   ~ 0
B64_IO_11
Text Label 3800 3800 0    50   ~ 0
B64_IO_12
Text Label 3800 3900 0    50   ~ 0
B64_IO_13
Text Label 3800 4000 0    50   ~ 0
B64_IO_14
Text Label 3800 4100 0    50   ~ 0
B64_IO_15
Text Label 3800 4200 0    50   ~ 0
B64_IO_16
Text Label 3800 4300 0    50   ~ 0
B64_IO_17
Text Label 3800 4400 0    50   ~ 0
B64_IO_18
Text Label 3800 4500 0    50   ~ 0
B64_IO_19
Text Label 3800 4600 0    50   ~ 0
B64_IO_20
Text Label 3800 4700 0    50   ~ 0
B64_IO_21
Text Label 3800 4800 0    50   ~ 0
B64_IO_22
Text Label 3800 4900 0    50   ~ 0
B64_IO_23
Text Label 3800 5000 0    50   ~ 0
B64_IO_24
Text Label 3800 5100 0    50   ~ 0
B64_IO_25
Text Label 8600 2600 2    50   ~ 0
B64_IO_26
Text Label 8600 2700 2    50   ~ 0
B64_IO_27
Text Label 8600 2800 2    50   ~ 0
B64_IO_28
Text Label 8600 2900 2    50   ~ 0
B64_IO_29
Text Label 8600 3000 2    50   ~ 0
B64_IO_30
Text Label 8600 3100 2    50   ~ 0
B64_IO_31
Text Label 8600 3200 2    50   ~ 0
B64_IO_32
Text Label 8600 3300 2    50   ~ 0
B64_IO_33
Text Label 8600 3400 2    50   ~ 0
B64_IO_34
Text Label 8600 3500 2    50   ~ 0
B64_IO_35
Text Label 8600 3600 2    50   ~ 0
B64_IO_36
Text Label 8600 3700 2    50   ~ 0
B64_IO_37
Text Label 8600 3800 2    50   ~ 0
B64_IO_38
Text Label 8600 3900 2    50   ~ 0
B64_IO_39
Text Label 8600 4000 2    50   ~ 0
B64_IO_40
Text Label 8600 4100 2    50   ~ 0
B64_IO_41
Text Label 8600 4200 2    50   ~ 0
B64_IO_42
Text Label 8600 4300 2    50   ~ 0
B64_IO_43
Text Label 8600 4400 2    50   ~ 0
B64_IO_44
Text Label 8600 4500 2    50   ~ 0
B64_IO_45
Text Label 8600 4600 2    50   ~ 0
B64_IO_46
Text Label 8600 4700 2    50   ~ 0
B64_IO_47
Text Label 8600 4800 2    50   ~ 0
B64_IO_48
Text Label 8600 4900 2    50   ~ 0
B64_IO_49
Text Label 8600 5000 2    50   ~ 0
B64_IO_50
Text Label 8600 5100 2    50   ~ 0
B64_IO_51
Wire Wire Line
	8650 2600 8000 2600
Wire Wire Line
	8650 2700 8000 2700
Wire Wire Line
	8650 2800 8000 2800
Wire Wire Line
	8650 2900 8000 2900
Wire Wire Line
	8650 3000 8000 3000
Wire Wire Line
	8650 3100 8000 3100
Wire Wire Line
	8650 3200 8000 3200
Wire Wire Line
	8650 3300 8000 3300
Wire Wire Line
	8650 3400 8000 3400
Wire Wire Line
	8650 3500 8000 3500
Wire Wire Line
	8650 3600 8000 3600
Wire Wire Line
	8650 3700 8000 3700
Wire Wire Line
	8650 3800 8000 3800
Wire Wire Line
	8650 3900 8000 3900
Wire Wire Line
	8650 4000 8000 4000
Wire Wire Line
	8650 4100 8000 4100
Wire Wire Line
	8650 4200 8000 4200
Wire Wire Line
	8650 4300 8000 4300
Wire Wire Line
	8650 4400 8000 4400
Wire Wire Line
	8650 4500 8000 4500
Wire Wire Line
	8650 4600 8000 4600
Wire Wire Line
	8650 4700 8000 4700
Wire Wire Line
	8650 4800 8000 4800
Wire Wire Line
	8650 4900 8000 4900
Wire Wire Line
	8650 5000 8000 5000
Wire Wire Line
	8650 5100 8000 5100
Wire Wire Line
	3750 2600 4400 2600
Wire Wire Line
	3750 2700 4400 2700
Wire Wire Line
	3750 2800 4400 2800
Wire Wire Line
	3750 2900 4400 2900
Wire Wire Line
	3750 3000 4400 3000
Wire Wire Line
	3750 3100 4400 3100
Wire Wire Line
	3750 3200 4400 3200
Wire Wire Line
	3750 3300 4400 3300
Wire Wire Line
	3750 3400 4400 3400
Wire Wire Line
	3750 3500 4400 3500
Wire Wire Line
	3750 3600 4400 3600
Wire Wire Line
	3750 3700 4400 3700
Wire Wire Line
	3750 3800 4400 3800
Wire Wire Line
	3750 3900 4400 3900
Wire Wire Line
	3750 4000 4400 4000
Wire Wire Line
	3750 4100 4400 4100
Wire Wire Line
	3750 4200 4400 4200
Wire Wire Line
	3750 4300 4400 4300
Wire Wire Line
	3750 4400 4400 4400
Wire Wire Line
	3750 4500 4400 4500
Wire Wire Line
	3750 4600 4400 4600
Wire Wire Line
	3750 4700 4400 4700
Wire Wire Line
	3750 4800 4400 4800
Wire Wire Line
	3750 4900 4400 4900
Wire Wire Line
	3750 5000 4400 5000
Wire Wire Line
	3750 5100 4400 5100
Entry Wire Line
	8650 2600 8750 2700
Entry Wire Line
	8650 2700 8750 2800
Entry Wire Line
	8650 2800 8750 2900
Entry Wire Line
	8650 2900 8750 3000
Entry Wire Line
	8650 3000 8750 3100
Entry Wire Line
	8650 3100 8750 3200
Entry Wire Line
	8650 3200 8750 3300
Entry Wire Line
	8650 3300 8750 3400
Entry Wire Line
	8650 3400 8750 3500
Entry Wire Line
	8650 3500 8750 3600
Entry Wire Line
	8650 3600 8750 3700
Entry Wire Line
	8650 3800 8750 3900
Entry Wire Line
	8650 3900 8750 4000
Entry Wire Line
	8650 4000 8750 4100
Entry Wire Line
	8650 4100 8750 4200
Entry Wire Line
	8650 4200 8750 4300
Entry Wire Line
	8650 4300 8750 4400
Entry Wire Line
	8650 4400 8750 4500
Entry Wire Line
	8650 4500 8750 4600
Entry Wire Line
	8650 4600 8750 4700
Entry Wire Line
	8650 4700 8750 4800
Entry Wire Line
	8650 4800 8750 4900
Entry Wire Line
	8650 4900 8750 5000
Entry Wire Line
	8650 5000 8750 5100
Entry Wire Line
	8650 5100 8750 5200
Entry Wire Line
	3650 5200 3750 5100
Entry Wire Line
	3650 5100 3750 5000
Entry Wire Line
	3650 5000 3750 4900
Entry Wire Line
	3650 4900 3750 4800
Entry Wire Line
	3650 4800 3750 4700
Entry Wire Line
	3650 4700 3750 4600
Entry Wire Line
	3650 4600 3750 4500
Entry Wire Line
	3650 4500 3750 4400
Entry Wire Line
	3650 4400 3750 4300
Entry Wire Line
	3650 4300 3750 4200
Entry Wire Line
	3650 4200 3750 4100
Entry Wire Line
	3650 4100 3750 4000
Entry Wire Line
	3650 4000 3750 3900
Entry Wire Line
	3650 3900 3750 3800
Entry Wire Line
	3650 3800 3750 3700
Entry Wire Line
	3650 3700 3750 3600
Entry Wire Line
	3650 3600 3750 3500
Entry Wire Line
	3650 3500 3750 3400
Entry Wire Line
	3650 3400 3750 3300
Entry Wire Line
	3650 3300 3750 3200
Entry Wire Line
	3650 3200 3750 3100
Entry Wire Line
	3650 3100 3750 3000
Entry Wire Line
	3650 3000 3750 2900
Entry Wire Line
	3650 2900 3750 2800
Entry Wire Line
	3650 2800 3750 2700
Entry Wire Line
	3650 2700 3750 2600
Wire Bus Line
	8750 5650 3650 5650
Connection ~ 3650 5650
Wire Bus Line
	3650 5650 2800 5650
Text GLabel 2800 5650 0    50   BiDi ~ 0
B64_IO_[0..51]
$Comp
L Device:C C?
U 1 1 5F043559
P 1750 2100
AR Path="/5E09AE40/5F043559" Ref="C?"  Part="1" 
AR Path="/5F3D10BF/5F043559" Ref="C278"  Part="1" 
F 0 "C278" H 1865 2146 50  0000 L CNN
F 1 "10uF" H 1865 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1788 1950 50  0001 C CNN
F 3 "~" H 1750 2100 50  0001 C CNN
F 4 "10µF ±20% 6.3V Ceramic Capacitor X6S 0402 (1005 Metric) " H 1750 2100 50  0001 C CNN "info"
F 5 "Murata" H 1750 2100 50  0001 C CNN "manf"
F 6 "GRM155C80J106ME11D" H 1750 2100 50  0001 C CNN "manf#"
	1    1750 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1900 1750 1900
Wire Wire Line
	1750 1900 1750 1950
Wire Wire Line
	1750 2300 2200 2300
Wire Wire Line
	1750 2250 1750 2300
Connection ~ 1750 1900
Wire Wire Line
	3100 2300 2650 2300
Connection ~ 2200 2300
Connection ~ 2650 2300
Wire Wire Line
	2650 2300 2200 2300
Wire Wire Line
	1750 2350 1750 2300
Connection ~ 1750 2300
NoConn ~ 8650 3700
Wire Bus Line
	8750 2700 8750 5650
Wire Bus Line
	3650 2700 3650 5650
$EndSCHEMATC
