EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 28
Title "ZynqMP Mezzanine FMC+ (BuZZyBoard)"
Date "2020-10-02"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "Institute for Data Processing and Electronics (IPE)"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 7 1 5F4122BD
P 2850 6000
F 0 "U1" H 3575 6290 60  0000 C CNN
F 1 "ZU4EG-B900" H 3575 6184 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 1150 5800 50  0001 C CNN
F 3 "" H 1150 5800 50  0001 C CNN
	7    2850 6000
	1    0    0    -1  
$EndComp
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 8 1 5F41F5E7
P 2850 4350
F 0 "U1" H 3575 4640 60  0000 C CNN
F 1 "ZU4EG-B900" H 3575 4534 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 1150 4150 50  0001 C CNN
F 3 "" H 1150 4150 50  0001 C CNN
	8    2850 4350
	1    0    0    -1  
$EndComp
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 9 1 5F881C1D
P 2850 2700
F 0 "U1" H 3575 2990 60  0000 C CNN
F 1 "ZU4EG-B900" H 3575 2884 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 1150 2500 50  0001 C CNN
F 3 "" H 1150 2500 50  0001 C CNN
	9    2850 2700
	1    0    0    -1  
$EndComp
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 10 1 5F44947E
P 2800 1050
F 0 "U1" H 3550 1340 60  0000 C CNN
F 1 "ZU4EG-B900" H 3550 1234 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 1100 850 50  0001 C CNN
F 3 "" H 1100 850 50  0001 C CNN
	10   2800 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1050 2300 1050
Wire Wire Line
	2150 1150 2300 1150
Wire Wire Line
	4800 1050 4950 1050
Wire Wire Line
	4800 1150 4950 1150
Wire Wire Line
	4850 2700 5000 2700
Wire Wire Line
	4850 2800 5000 2800
Wire Wire Line
	2150 2700 2300 2700
Wire Wire Line
	2150 2800 2300 2800
Wire Wire Line
	2150 4350 2300 4350
Wire Wire Line
	2150 4450 2300 4450
Wire Wire Line
	4850 4350 5000 4350
Wire Wire Line
	4850 4450 5000 4450
Wire Wire Line
	4850 6000 5000 6000
Wire Wire Line
	4850 6100 5000 6100
Wire Wire Line
	2150 6000 2300 6000
Wire Wire Line
	2150 6100 2300 6100
Text GLabel 5000 6500 2    50   Output ~ 0
PL_MGT_TX_P0
Text GLabel 5000 6700 2    50   Output ~ 0
PL_MGT_TX_P1
Text GLabel 5000 6900 2    50   Output ~ 0
PL_MGT_TX_P2
Text GLabel 5000 7100 2    50   Output ~ 0
PL_MGT_TX_P3
Text GLabel 5000 6400 2    50   Output ~ 0
PL_MGT_TX_N0
Text GLabel 5000 6600 2    50   Output ~ 0
PL_MGT_TX_N1
Text GLabel 5000 6800 2    50   Output ~ 0
PL_MGT_TX_N2
Text GLabel 5000 7000 2    50   Output ~ 0
PL_MGT_TX_N3
Wire Wire Line
	5000 6400 4850 6400
Wire Wire Line
	5000 6500 4850 6500
Wire Wire Line
	5000 6600 4850 6600
Wire Wire Line
	5000 6700 4850 6700
Wire Wire Line
	5000 6800 4850 6800
Wire Wire Line
	5000 6900 4850 6900
Wire Wire Line
	5000 7000 4850 7000
Wire Wire Line
	5000 7100 4850 7100
Text GLabel 5000 4750 2    50   Output ~ 0
PL_MGT_TX_N4
Text GLabel 5000 4950 2    50   Output ~ 0
PL_MGT_TX_N5
Text GLabel 5000 5150 2    50   Output ~ 0
PL_MGT_TX_N6
Text GLabel 5000 5350 2    50   Output ~ 0
PL_MGT_TX_N7
Text GLabel 5000 3100 2    50   Output ~ 0
PL_MGT_TX_N8
Text GLabel 5000 3300 2    50   Output ~ 0
PL_MGT_TX_N9
Text GLabel 5000 3500 2    50   Output ~ 0
PL_MGT_TX_N10
Text GLabel 5000 3700 2    50   Output ~ 0
PL_MGT_TX_N11
Text GLabel 4950 1450 2    50   Output ~ 0
PL_MGT_TX_N12
Text GLabel 4950 1650 2    50   Output ~ 0
PL_MGT_TX_N13
Text GLabel 4950 1850 2    50   Output ~ 0
PL_MGT_TX_N14
Text GLabel 4950 2050 2    50   Output ~ 0
PL_MGT_TX_N15
Text GLabel 5000 4850 2    50   Output ~ 0
PL_MGT_TX_P4
Text GLabel 5000 5050 2    50   Output ~ 0
PL_MGT_TX_P5
Text GLabel 5000 5250 2    50   Output ~ 0
PL_MGT_TX_P6
Text GLabel 5000 5450 2    50   Output ~ 0
PL_MGT_TX_P7
Text GLabel 5000 3200 2    50   Output ~ 0
PL_MGT_TX_P8
Text GLabel 5000 3400 2    50   Output ~ 0
PL_MGT_TX_P9
Text GLabel 5000 3600 2    50   Output ~ 0
PL_MGT_TX_P10
Text GLabel 5000 3800 2    50   Output ~ 0
PL_MGT_TX_P11
Text GLabel 4950 1550 2    50   Output ~ 0
PL_MGT_TX_P12
Text GLabel 4950 1750 2    50   Output ~ 0
PL_MGT_TX_P13
Text GLabel 4950 1950 2    50   Output ~ 0
PL_MGT_TX_P14
Text GLabel 4950 2150 2    50   Output ~ 0
PL_MGT_TX_P15
Text GLabel 2150 4750 0    50   Input ~ 0
PL_MGT_RX_N4
Text GLabel 2150 4950 0    50   Input ~ 0
PL_MGT_RX_N5
Text GLabel 2150 5150 0    50   Input ~ 0
PL_MGT_RX_N6
Text GLabel 2150 5350 0    50   Input ~ 0
PL_MGT_RX_N7
Text GLabel 2150 3100 0    50   Input ~ 0
PL_MGT_RX_N8
Text GLabel 2150 3300 0    50   Input ~ 0
PL_MGT_RX_N9
Text GLabel 2150 3500 0    50   Input ~ 0
PL_MGT_RX_N10
Text GLabel 2150 3700 0    50   Input ~ 0
PL_MGT_RX_N11
Text GLabel 2150 1450 0    50   Input ~ 0
PL_MGT_RX_N12
Text GLabel 2150 1650 0    50   Input ~ 0
PL_MGT_RX_N13
Text GLabel 2150 1850 0    50   Input ~ 0
PL_MGT_RX_N14
Text GLabel 2150 2050 0    50   Input ~ 0
PL_MGT_RX_N15
Text GLabel 2150 4850 0    50   Input ~ 0
PL_MGT_RX_P4
Text GLabel 2150 5050 0    50   Input ~ 0
PL_MGT_RX_P5
Text GLabel 2150 5250 0    50   Input ~ 0
PL_MGT_RX_P6
Text GLabel 2150 5450 0    50   Input ~ 0
PL_MGT_RX_P7
Text GLabel 2150 3200 0    50   Input ~ 0
PL_MGT_RX_P8
Text GLabel 2150 3400 0    50   Input ~ 0
PL_MGT_RX_P9
Text GLabel 2150 3600 0    50   Input ~ 0
PL_MGT_RX_P10
Text GLabel 2150 3800 0    50   Input ~ 0
PL_MGT_RX_P11
Text GLabel 2150 1550 0    50   Input ~ 0
PL_MGT_RX_P12
Text GLabel 2150 1750 0    50   Input ~ 0
PL_MGT_RX_P13
Text GLabel 2150 1950 0    50   Input ~ 0
PL_MGT_RX_P14
Text GLabel 2150 2150 0    50   Input ~ 0
PL_MGT_RX_P15
Text GLabel 2150 7100 0    50   Input ~ 0
PL_MGT_RX_P3
Text GLabel 2150 6900 0    50   Input ~ 0
PL_MGT_RX_P2
Text GLabel 2150 6700 0    50   Input ~ 0
PL_MGT_RX_P1
Text GLabel 2150 6500 0    50   Input ~ 0
PL_MGT_RX_P0
Text GLabel 2150 7000 0    50   Input ~ 0
PL_MGT_RX_N3
Text GLabel 2150 6800 0    50   Input ~ 0
PL_MGT_RX_N2
Text GLabel 2150 6600 0    50   Input ~ 0
PL_MGT_RX_N1
Text GLabel 2150 6400 0    50   Input ~ 0
PL_MGT_RX_N0
Wire Wire Line
	2150 6400 2300 6400
Wire Wire Line
	2150 6500 2300 6500
Wire Wire Line
	2150 6600 2300 6600
Wire Wire Line
	2150 6700 2300 6700
Wire Wire Line
	2150 6800 2300 6800
Wire Wire Line
	2150 6900 2300 6900
Wire Wire Line
	2150 7000 2300 7000
Wire Wire Line
	2150 7100 2300 7100
Wire Wire Line
	4850 4750 5000 4750
Wire Wire Line
	4850 4850 5000 4850
Wire Wire Line
	4850 4950 5000 4950
Wire Wire Line
	4850 5050 5000 5050
Wire Wire Line
	4850 5150 5000 5150
Wire Wire Line
	4850 5250 5000 5250
Wire Wire Line
	4850 5350 5000 5350
Wire Wire Line
	4850 5450 5000 5450
Wire Wire Line
	2150 4750 2300 4750
Wire Wire Line
	2150 4850 2300 4850
Wire Wire Line
	2150 4950 2300 4950
Wire Wire Line
	2150 5050 2300 5050
Wire Wire Line
	2150 5150 2300 5150
Wire Wire Line
	2150 5250 2300 5250
Wire Wire Line
	2150 5350 2300 5350
Wire Wire Line
	2150 5450 2300 5450
Wire Wire Line
	2150 1450 2300 1450
Wire Wire Line
	2150 1550 2300 1550
Wire Wire Line
	2150 1650 2300 1650
Wire Wire Line
	2150 1750 2300 1750
Wire Wire Line
	2150 1850 2300 1850
Wire Wire Line
	2150 1950 2300 1950
Wire Wire Line
	2150 2050 2300 2050
Wire Wire Line
	2150 2150 2300 2150
Wire Wire Line
	4800 1450 4950 1450
Wire Wire Line
	4800 1550 4950 1550
Wire Wire Line
	4800 1650 4950 1650
Wire Wire Line
	4800 1750 4950 1750
Wire Wire Line
	4800 1850 4950 1850
Wire Wire Line
	4800 1950 4950 1950
Wire Wire Line
	4800 2050 4950 2050
Wire Wire Line
	4800 2150 4950 2150
Wire Wire Line
	2150 3100 2300 3100
Wire Wire Line
	2150 3200 2300 3200
Wire Wire Line
	2150 3300 2300 3300
Wire Wire Line
	2150 3400 2300 3400
Wire Wire Line
	2150 3500 2300 3500
Wire Wire Line
	2150 3600 2300 3600
Wire Wire Line
	2150 3700 2300 3700
Wire Wire Line
	2150 3800 2300 3800
Wire Wire Line
	4850 3100 5000 3100
Wire Wire Line
	4850 3200 5000 3200
Wire Wire Line
	4850 3300 5000 3300
Wire Wire Line
	4850 3400 5000 3400
Wire Wire Line
	4850 3500 5000 3500
Wire Wire Line
	4850 3600 5000 3600
Wire Wire Line
	4850 3700 5000 3700
Wire Wire Line
	4850 3800 5000 3800
Text GLabel 2150 6000 0    50   BiDi ~ 0
PL_MGT_CLK_N0
Text GLabel 5000 6000 2    50   BiDi ~ 0
PL_MGT_CLK_N1
Text GLabel 2150 4350 0    50   BiDi ~ 0
PL_MGT_CLK_N2
Text GLabel 5000 4350 2    50   BiDi ~ 0
PL_MGT_CLK_N3
Text GLabel 2150 2700 0    50   BiDi ~ 0
PL_MGT_CLK_N4
Text GLabel 5000 2700 2    50   BiDi ~ 0
PL_MGT_CLK_N5
Text GLabel 2150 1050 0    50   BiDi ~ 0
PL_MGT_CLK_N6
Text GLabel 4950 1050 2    50   BiDi ~ 0
PL_MGT_CLK_N7
Text GLabel 2150 6100 0    50   BiDi ~ 0
PL_MGT_CLK_P0
Text GLabel 5000 6100 2    50   BiDi ~ 0
PL_MGT_CLK_P1
Text GLabel 2150 4450 0    50   BiDi ~ 0
PL_MGT_CLK_P2
Text GLabel 5000 4450 2    50   BiDi ~ 0
PL_MGT_CLK_P3
Text GLabel 2150 2800 0    50   BiDi ~ 0
PL_MGT_CLK_P4
Text GLabel 5000 2800 2    50   BiDi ~ 0
PL_MGT_CLK_P5
Text GLabel 2150 1150 0    50   BiDi ~ 0
PL_MGT_CLK_P6
Text GLabel 4950 1150 2    50   BiDi ~ 0
PL_MGT_CLK_P7
$EndSCHEMATC
