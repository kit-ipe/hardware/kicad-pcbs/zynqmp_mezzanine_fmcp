EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 28
Title "ZynqMP Mezzanine FMC+ (BuZZyBoard)"
Date "2020-10-02"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "Institute for Data Processing and Electronics (IPE)"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L KIT_FPGA:ZU4EG-B900 U1
U 17 1 5F78F330
P 5000 2800
F 0 "U1" H 5900 3090 60  0000 C CNN
F 1 "ZU4EG-B900" H 5900 2984 60  0000 C CNN
F 2 "KIT_Package_BGA:BGA-900_30x30_31.0x31.0mm" H 3300 2600 50  0001 C CNN
F 3 "" H 3300 2600 50  0001 C CNN
	17   5000 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2800 7300 2800
Wire Wire Line
	7300 2900 7450 2900
Wire Wire Line
	4500 2800 4350 2800
Wire Wire Line
	4500 2900 4350 2900
Wire Wire Line
	4500 3000 4350 3000
Wire Wire Line
	4500 3100 4350 3100
$Comp
L Device:R R1
U 1 1 5DEC60BF
P 8600 3400
F 0 "R1" V 8303 3400 50  0000 C CNN
F 1 "500" V 8394 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 8530 3400 50  0001 C CNN
F 3 "~" H 8600 3400 50  0001 C CNN
F 4 "0.5%" V 8485 3400 50  0000 C CNN "info"
	1    8600 3400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DEC7E26
P 8850 3400
F 0 "#PWR?" H 8850 3150 50  0001 C CNN
F 1 "GND" H 8855 3227 50  0000 C CNN
F 2 "" H 8850 3400 50  0001 C CNN
F 3 "" H 8850 3400 50  0001 C CNN
	1    8850 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 3400 8750 3400
Wire Wire Line
	8450 3400 7300 3400
Wire Wire Line
	7450 3000 7300 3000
Wire Wire Line
	7300 3100 7450 3100
Text Label 7450 3400 0    50   ~ 0
PS_MGTRREF
Text GLabel 4350 2800 0    50   BiDi ~ 0
PS_MGT_CLK_N0
Text GLabel 4350 3000 0    50   BiDi ~ 0
PS_MGT_CLK_N1
Text GLabel 7450 2800 2    50   BiDi ~ 0
PS_MGT_CLK_N2
Text GLabel 7450 3000 2    50   BiDi ~ 0
PS_MGT_CLK_N3
Text GLabel 4350 2900 0    50   BiDi ~ 0
PS_MGT_CLK_P0
Text GLabel 4350 3100 0    50   BiDi ~ 0
PS_MGT_CLK_P1
Text GLabel 7450 2900 2    50   BiDi ~ 0
PS_MGT_CLK_P2
Text GLabel 7450 3100 2    50   BiDi ~ 0
PS_MGT_CLK_P3
Text GLabel 7450 3700 2    50   Output ~ 0
PS_MGT_TX_P0
Text GLabel 7450 3900 2    50   Output ~ 0
PS_MGT_TX_P1
Text GLabel 7450 4100 2    50   Output ~ 0
PS_MGT_TX_P2
Text GLabel 7450 4300 2    50   Output ~ 0
PS_MGT_TX_P3
Text GLabel 7450 3600 2    50   Output ~ 0
PS_MGT_TX_N0
Text GLabel 7450 3800 2    50   Output ~ 0
PS_MGT_TX_N1
Text GLabel 7450 4000 2    50   Output ~ 0
PS_MGT_TX_N2
Text GLabel 7450 4200 2    50   Output ~ 0
PS_MGT_TX_N3
Wire Wire Line
	7450 3600 7300 3600
Wire Wire Line
	7450 3700 7300 3700
Wire Wire Line
	7450 3800 7300 3800
Wire Wire Line
	7450 3900 7300 3900
Wire Wire Line
	7450 4000 7300 4000
Wire Wire Line
	7450 4100 7300 4100
Wire Wire Line
	7450 4200 7300 4200
Wire Wire Line
	7450 4300 7300 4300
Text GLabel 4350 4300 0    50   Input ~ 0
PS_MGT_RX_P3
Text GLabel 4350 4100 0    50   Input ~ 0
PS_MGT_RX_P2
Text GLabel 4350 3900 0    50   Input ~ 0
PS_MGT_RX_P1
Text GLabel 4350 3700 0    50   Input ~ 0
PS_MGT_RX_P0
Text GLabel 4350 4200 0    50   Input ~ 0
PS_MGT_RX_N3
Text GLabel 4350 4000 0    50   Input ~ 0
PS_MGT_RX_N2
Text GLabel 4350 3800 0    50   Input ~ 0
PS_MGT_RX_N1
Text GLabel 4350 3600 0    50   Input ~ 0
PS_MGT_RX_N0
Wire Wire Line
	4350 3600 4500 3600
Wire Wire Line
	4350 3700 4500 3700
Wire Wire Line
	4350 3800 4500 3800
Wire Wire Line
	4350 3900 4500 3900
Wire Wire Line
	4350 4000 4500 4000
Wire Wire Line
	4350 4100 4500 4100
Wire Wire Line
	4350 4200 4500 4200
Wire Wire Line
	4350 4300 4500 4300
$EndSCHEMATC
