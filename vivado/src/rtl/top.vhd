----------------------------------------------------------------------------------
-- company:
-- engineer:
--
-- create date: 03/10/2020 05:14:21 pm
-- design name:
-- module name: trenz_module_top - behavioral
-- project name:
-- target devices:
-- tool versions:
-- description:
--
-- dependencies:
--
-- revision:
-- revision 0.01 - file created
-- additional comments:
--
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

-- uncomment the following library declaration if using
-- arithmetic functions with signed or unsigned values
--use ieee.numeric_std.all;

-- uncomment the following library declaration if instantiating
-- any xilinx leaf cells in this code.
--library unisim;
--use unisim.vcomponents.all;

entity top is
 port (
    rgmii_rst_n     : out std_logic;
    rgmii_rx_data   : out std_logic_vector(3 downto 0);
    rgmii_rx_clk    : out std_logic;
    rgmii_rx_ctl    : out std_logic;
    rgmii_tx_data   : in  std_logic_vector(3 downto 0);
    rgmii_tx_clk    : in  std_logic;
    rgmii_tx_ctl    : in  std_logic;
    rgmii_mdio      : inout std_logic;
    rgmii_mdc       : out std_logic
 );
end entity top;

architecture rtl of top is

      

begin




end rtl;
