--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
--Date        : Wed Apr 22 15:46:21 2020
--Host        : Sager running 64-bit Linux Mint 19.3 Tricia
--Command     : generate_target zusys_wrapper.bd
--Design      : zusys_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity zusys_wrapper is
  port (
    AXI_ACLK : out STD_LOGIC;
    GMII_ENET3_0_col : in STD_LOGIC;
    GMII_ENET3_0_crs : in STD_LOGIC;
    GMII_ENET3_0_rx_clk : in STD_LOGIC;
    GMII_ENET3_0_rx_dv : in STD_LOGIC;
    GMII_ENET3_0_rx_er : in STD_LOGIC;
    GMII_ENET3_0_rxd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    GMII_ENET3_0_speed_mode : out STD_LOGIC_VECTOR ( 2 downto 0 );
    GMII_ENET3_0_tx_clk : in STD_LOGIC;
    GMII_ENET3_0_tx_en : out STD_LOGIC;
    GMII_ENET3_0_tx_er : out STD_LOGIC;
    GMII_ENET3_0_txd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M00_AXI_0_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M00_AXI_0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_arid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M00_AXI_0_arlock : out STD_LOGIC;
    M00_AXI_0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_arready : in STD_LOGIC;
    M00_AXI_0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_aruser : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_arvalid : out STD_LOGIC;
    M00_AXI_0_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M00_AXI_0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_awid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M00_AXI_0_awlock : out STD_LOGIC;
    M00_AXI_0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_awready : in STD_LOGIC;
    M00_AXI_0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_awuser : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_awvalid : out STD_LOGIC;
    M00_AXI_0_bid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_bready : out STD_LOGIC;
    M00_AXI_0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_bvalid : in STD_LOGIC;
    M00_AXI_0_rdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    M00_AXI_0_rid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_rlast : in STD_LOGIC;
    M00_AXI_0_rready : out STD_LOGIC;
    M00_AXI_0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_rvalid : in STD_LOGIC;
    M00_AXI_0_wdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
    M00_AXI_0_wlast : out STD_LOGIC;
    M00_AXI_0_wready : in STD_LOGIC;
    M00_AXI_0_wstrb : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_wvalid : out STD_LOGIC;
    MDIO_ENET0_0_mdc : out STD_LOGIC;
    MDIO_ENET0_0_mdio_io : inout STD_LOGIC;
    MDIO_ENET3_0_mdc : out STD_LOGIC;
    MDIO_ENET3_0_mdio_io : inout STD_LOGIC;
    SPI_0_0_io0_io : inout STD_LOGIC;
    SPI_0_0_io1_io : inout STD_LOGIC;
    SPI_0_0_sck_io : inout STD_LOGIC;
    SPI_0_0_ss1_o : out STD_LOGIC;
    SPI_0_0_ss2_o : out STD_LOGIC;
    SPI_0_0_ss_io : inout STD_LOGIC;
    iic_rtl_0_scl_io : inout STD_LOGIC;
    iic_rtl_0_sda_io : inout STD_LOGIC
  );
end zusys_wrapper;

architecture STRUCTURE of zusys_wrapper is
  component zusys is
  port (
    GMII_ENET3_0_rx_clk : in STD_LOGIC;
    GMII_ENET3_0_speed_mode : out STD_LOGIC_VECTOR ( 2 downto 0 );
    GMII_ENET3_0_crs : in STD_LOGIC;
    GMII_ENET3_0_col : in STD_LOGIC;
    GMII_ENET3_0_rxd : in STD_LOGIC_VECTOR ( 7 downto 0 );
    GMII_ENET3_0_rx_er : in STD_LOGIC;
    GMII_ENET3_0_rx_dv : in STD_LOGIC;
    GMII_ENET3_0_tx_clk : in STD_LOGIC;
    GMII_ENET3_0_txd : out STD_LOGIC_VECTOR ( 7 downto 0 );
    GMII_ENET3_0_tx_en : out STD_LOGIC;
    GMII_ENET3_0_tx_er : out STD_LOGIC;
    MDIO_ENET0_0_mdc : out STD_LOGIC;
    MDIO_ENET0_0_mdio_i : in STD_LOGIC;
    MDIO_ENET0_0_mdio_o : out STD_LOGIC;
    MDIO_ENET0_0_mdio_t : out STD_LOGIC;
    MDIO_ENET3_0_mdc : out STD_LOGIC;
    MDIO_ENET3_0_mdio_i : in STD_LOGIC;
    MDIO_ENET3_0_mdio_o : out STD_LOGIC;
    MDIO_ENET3_0_mdio_t : out STD_LOGIC;
    SPI_0_0_sck_i : in STD_LOGIC;
    SPI_0_0_sck_o : out STD_LOGIC;
    SPI_0_0_sck_t : out STD_LOGIC;
    SPI_0_0_io0_i : in STD_LOGIC;
    SPI_0_0_io0_o : out STD_LOGIC;
    SPI_0_0_io0_t : out STD_LOGIC;
    SPI_0_0_io1_i : in STD_LOGIC;
    SPI_0_0_io1_o : out STD_LOGIC;
    SPI_0_0_io1_t : out STD_LOGIC;
    SPI_0_0_ss_i : in STD_LOGIC;
    SPI_0_0_ss_o : out STD_LOGIC;
    SPI_0_0_ss1_o : out STD_LOGIC;
    SPI_0_0_ss2_o : out STD_LOGIC;
    SPI_0_0_ss_t : out STD_LOGIC;
    iic_rtl_0_scl_i : in STD_LOGIC;
    iic_rtl_0_scl_o : out STD_LOGIC;
    iic_rtl_0_scl_t : out STD_LOGIC;
    iic_rtl_0_sda_i : in STD_LOGIC;
    iic_rtl_0_sda_o : out STD_LOGIC;
    iic_rtl_0_sda_t : out STD_LOGIC;
    M00_AXI_0_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M00_AXI_0_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M00_AXI_0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_awlock : out STD_LOGIC;
    M00_AXI_0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_awvalid : out STD_LOGIC;
    M00_AXI_0_awready : in STD_LOGIC;
    M00_AXI_0_wdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
    M00_AXI_0_wstrb : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_wlast : out STD_LOGIC;
    M00_AXI_0_wvalid : out STD_LOGIC;
    M00_AXI_0_wready : in STD_LOGIC;
    M00_AXI_0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_bvalid : in STD_LOGIC;
    M00_AXI_0_bready : out STD_LOGIC;
    M00_AXI_0_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
    M00_AXI_0_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M00_AXI_0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_arlock : out STD_LOGIC;
    M00_AXI_0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M00_AXI_0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M00_AXI_0_arvalid : out STD_LOGIC;
    M00_AXI_0_arready : in STD_LOGIC;
    M00_AXI_0_rdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    M00_AXI_0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M00_AXI_0_rlast : in STD_LOGIC;
    M00_AXI_0_rvalid : in STD_LOGIC;
    M00_AXI_0_rready : out STD_LOGIC;
    M00_AXI_0_arid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_aruser : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_awid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_awuser : out STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_bid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    M00_AXI_0_rid : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI_ACLK : out STD_LOGIC
  );
  end component zusys;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal MDIO_ENET0_0_mdio_i : STD_LOGIC;
  signal MDIO_ENET0_0_mdio_o : STD_LOGIC;
  signal MDIO_ENET0_0_mdio_t : STD_LOGIC;
  signal MDIO_ENET3_0_mdio_i : STD_LOGIC;
  signal MDIO_ENET3_0_mdio_o : STD_LOGIC;
  signal MDIO_ENET3_0_mdio_t : STD_LOGIC;
  signal SPI_0_0_io0_i : STD_LOGIC;
  signal SPI_0_0_io0_o : STD_LOGIC;
  signal SPI_0_0_io0_t : STD_LOGIC;
  signal SPI_0_0_io1_i : STD_LOGIC;
  signal SPI_0_0_io1_o : STD_LOGIC;
  signal SPI_0_0_io1_t : STD_LOGIC;
  signal SPI_0_0_sck_i : STD_LOGIC;
  signal SPI_0_0_sck_o : STD_LOGIC;
  signal SPI_0_0_sck_t : STD_LOGIC;
  signal SPI_0_0_ss_i : STD_LOGIC;
  signal SPI_0_0_ss_o : STD_LOGIC;
  signal SPI_0_0_ss_t : STD_LOGIC;
  signal iic_rtl_0_scl_i : STD_LOGIC;
  signal iic_rtl_0_scl_o : STD_LOGIC;
  signal iic_rtl_0_scl_t : STD_LOGIC;
  signal iic_rtl_0_sda_i : STD_LOGIC;
  signal iic_rtl_0_sda_o : STD_LOGIC;
  signal iic_rtl_0_sda_t : STD_LOGIC;
begin
MDIO_ENET0_0_mdio_iobuf: component IOBUF
     port map (
      I => MDIO_ENET0_0_mdio_o,
      IO => MDIO_ENET0_0_mdio_io,
      O => MDIO_ENET0_0_mdio_i,
      T => MDIO_ENET0_0_mdio_t
    );
MDIO_ENET3_0_mdio_iobuf: component IOBUF
     port map (
      I => MDIO_ENET3_0_mdio_o,
      IO => MDIO_ENET3_0_mdio_io,
      O => MDIO_ENET3_0_mdio_i,
      T => MDIO_ENET3_0_mdio_t
    );
SPI_0_0_io0_iobuf: component IOBUF
     port map (
      I => SPI_0_0_io0_o,
      IO => SPI_0_0_io0_io,
      O => SPI_0_0_io0_i,
      T => SPI_0_0_io0_t
    );
SPI_0_0_io1_iobuf: component IOBUF
     port map (
      I => SPI_0_0_io1_o,
      IO => SPI_0_0_io1_io,
      O => SPI_0_0_io1_i,
      T => SPI_0_0_io1_t
    );
SPI_0_0_sck_iobuf: component IOBUF
     port map (
      I => SPI_0_0_sck_o,
      IO => SPI_0_0_sck_io,
      O => SPI_0_0_sck_i,
      T => SPI_0_0_sck_t
    );
SPI_0_0_ss_iobuf: component IOBUF
     port map (
      I => SPI_0_0_ss_o,
      IO => SPI_0_0_ss_io,
      O => SPI_0_0_ss_i,
      T => SPI_0_0_ss_t
    );
iic_rtl_0_scl_iobuf: component IOBUF
     port map (
      I => iic_rtl_0_scl_o,
      IO => iic_rtl_0_scl_io,
      O => iic_rtl_0_scl_i,
      T => iic_rtl_0_scl_t
    );
iic_rtl_0_sda_iobuf: component IOBUF
     port map (
      I => iic_rtl_0_sda_o,
      IO => iic_rtl_0_sda_io,
      O => iic_rtl_0_sda_i,
      T => iic_rtl_0_sda_t
    );
zusys_i: component zusys
     port map (
      AXI_ACLK => AXI_ACLK,
      GMII_ENET3_0_col => GMII_ENET3_0_col,
      GMII_ENET3_0_crs => GMII_ENET3_0_crs,
      GMII_ENET3_0_rx_clk => GMII_ENET3_0_rx_clk,
      GMII_ENET3_0_rx_dv => GMII_ENET3_0_rx_dv,
      GMII_ENET3_0_rx_er => GMII_ENET3_0_rx_er,
      GMII_ENET3_0_rxd(7 downto 0) => GMII_ENET3_0_rxd(7 downto 0),
      GMII_ENET3_0_speed_mode(2 downto 0) => GMII_ENET3_0_speed_mode(2 downto 0),
      GMII_ENET3_0_tx_clk => GMII_ENET3_0_tx_clk,
      GMII_ENET3_0_tx_en => GMII_ENET3_0_tx_en,
      GMII_ENET3_0_tx_er => GMII_ENET3_0_tx_er,
      GMII_ENET3_0_txd(7 downto 0) => GMII_ENET3_0_txd(7 downto 0),
      M00_AXI_0_araddr(39 downto 0) => M00_AXI_0_araddr(39 downto 0),
      M00_AXI_0_arburst(1 downto 0) => M00_AXI_0_arburst(1 downto 0),
      M00_AXI_0_arcache(3 downto 0) => M00_AXI_0_arcache(3 downto 0),
      M00_AXI_0_arid(15 downto 0) => M00_AXI_0_arid(15 downto 0),
      M00_AXI_0_arlen(7 downto 0) => M00_AXI_0_arlen(7 downto 0),
      M00_AXI_0_arlock => M00_AXI_0_arlock,
      M00_AXI_0_arprot(2 downto 0) => M00_AXI_0_arprot(2 downto 0),
      M00_AXI_0_arqos(3 downto 0) => M00_AXI_0_arqos(3 downto 0),
      M00_AXI_0_arready => M00_AXI_0_arready,
      M00_AXI_0_arsize(2 downto 0) => M00_AXI_0_arsize(2 downto 0),
      M00_AXI_0_aruser(15 downto 0) => M00_AXI_0_aruser(15 downto 0),
      M00_AXI_0_arvalid => M00_AXI_0_arvalid,
      M00_AXI_0_awaddr(39 downto 0) => M00_AXI_0_awaddr(39 downto 0),
      M00_AXI_0_awburst(1 downto 0) => M00_AXI_0_awburst(1 downto 0),
      M00_AXI_0_awcache(3 downto 0) => M00_AXI_0_awcache(3 downto 0),
      M00_AXI_0_awid(15 downto 0) => M00_AXI_0_awid(15 downto 0),
      M00_AXI_0_awlen(7 downto 0) => M00_AXI_0_awlen(7 downto 0),
      M00_AXI_0_awlock => M00_AXI_0_awlock,
      M00_AXI_0_awprot(2 downto 0) => M00_AXI_0_awprot(2 downto 0),
      M00_AXI_0_awqos(3 downto 0) => M00_AXI_0_awqos(3 downto 0),
      M00_AXI_0_awready => M00_AXI_0_awready,
      M00_AXI_0_awsize(2 downto 0) => M00_AXI_0_awsize(2 downto 0),
      M00_AXI_0_awuser(15 downto 0) => M00_AXI_0_awuser(15 downto 0),
      M00_AXI_0_awvalid => M00_AXI_0_awvalid,
      M00_AXI_0_bid(15 downto 0) => M00_AXI_0_bid(15 downto 0),
      M00_AXI_0_bready => M00_AXI_0_bready,
      M00_AXI_0_bresp(1 downto 0) => M00_AXI_0_bresp(1 downto 0),
      M00_AXI_0_bvalid => M00_AXI_0_bvalid,
      M00_AXI_0_rdata(127 downto 0) => M00_AXI_0_rdata(127 downto 0),
      M00_AXI_0_rid(15 downto 0) => M00_AXI_0_rid(15 downto 0),
      M00_AXI_0_rlast => M00_AXI_0_rlast,
      M00_AXI_0_rready => M00_AXI_0_rready,
      M00_AXI_0_rresp(1 downto 0) => M00_AXI_0_rresp(1 downto 0),
      M00_AXI_0_rvalid => M00_AXI_0_rvalid,
      M00_AXI_0_wdata(127 downto 0) => M00_AXI_0_wdata(127 downto 0),
      M00_AXI_0_wlast => M00_AXI_0_wlast,
      M00_AXI_0_wready => M00_AXI_0_wready,
      M00_AXI_0_wstrb(15 downto 0) => M00_AXI_0_wstrb(15 downto 0),
      M00_AXI_0_wvalid => M00_AXI_0_wvalid,
      MDIO_ENET0_0_mdc => MDIO_ENET0_0_mdc,
      MDIO_ENET0_0_mdio_i => MDIO_ENET0_0_mdio_i,
      MDIO_ENET0_0_mdio_o => MDIO_ENET0_0_mdio_o,
      MDIO_ENET0_0_mdio_t => MDIO_ENET0_0_mdio_t,
      MDIO_ENET3_0_mdc => MDIO_ENET3_0_mdc,
      MDIO_ENET3_0_mdio_i => MDIO_ENET3_0_mdio_i,
      MDIO_ENET3_0_mdio_o => MDIO_ENET3_0_mdio_o,
      MDIO_ENET3_0_mdio_t => MDIO_ENET3_0_mdio_t,
      SPI_0_0_io0_i => SPI_0_0_io0_i,
      SPI_0_0_io0_o => SPI_0_0_io0_o,
      SPI_0_0_io0_t => SPI_0_0_io0_t,
      SPI_0_0_io1_i => SPI_0_0_io1_i,
      SPI_0_0_io1_o => SPI_0_0_io1_o,
      SPI_0_0_io1_t => SPI_0_0_io1_t,
      SPI_0_0_sck_i => SPI_0_0_sck_i,
      SPI_0_0_sck_o => SPI_0_0_sck_o,
      SPI_0_0_sck_t => SPI_0_0_sck_t,
      SPI_0_0_ss1_o => SPI_0_0_ss1_o,
      SPI_0_0_ss2_o => SPI_0_0_ss2_o,
      SPI_0_0_ss_i => SPI_0_0_ss_i,
      SPI_0_0_ss_o => SPI_0_0_ss_o,
      SPI_0_0_ss_t => SPI_0_0_ss_t,
      iic_rtl_0_scl_i => iic_rtl_0_scl_i,
      iic_rtl_0_scl_o => iic_rtl_0_scl_o,
      iic_rtl_0_scl_t => iic_rtl_0_scl_t,
      iic_rtl_0_sda_i => iic_rtl_0_sda_i,
      iic_rtl_0_sda_o => iic_rtl_0_sda_o,
      iic_rtl_0_sda_t => iic_rtl_0_sda_t
    );
end STRUCTURE;
