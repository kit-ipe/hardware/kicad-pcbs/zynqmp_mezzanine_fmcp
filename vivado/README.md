## Build Vivado Project

    `vivado -source zynqmp_mezzanine_fmcp.tcl &`

## Check Locale
- Check for the locale related variables "LC_" in the xinfo.txt
    `report_environment -file xinfo.txt`


### TODO
- configure SWDT
- configure TTC
